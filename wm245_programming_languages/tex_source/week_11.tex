\documentclass[11pt,oneside,a4paper]{article}
\usepackage[%
	a4paper,%
	left = 20mm,%
	right = 20mm,%
	textwidth = 178mm,%
	top = 35mm,%
	bottom = 30mm,%
	headheight=70pt,%
headsep=25pt,%
]{geometry}
\usepackage{graphicx}
\usepackage[inkscapeformat=png]{svg}
\usepackage{transparent}
\usepackage{lastpage}
\usepackage{exsheets}
\usepackage{fontspec}
\setmainfont{Montserrat}
\setmainfont{Noto Sans}

\input{helper/preamble.tex}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Edit below
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\def\weekno{11}
\def\labtitle{Event Programming and Multithreading in Java}
\input{module_info}

\setmainfont[
    UprightFont = {Montserrat Light},
    BoldFont = {Montserrat Bold},
    ItalicFont = {Montserrat Italic},
    BoldItalicFont = {Montserrat Bold Italic}
]{Montserrat}

\input{helper/header.tex}

\title{Week \weekno{}: \labtitle{}}
\author{\moduleno{} \moduletitle{}}
\date{January 2024}

\begin{document}

\begin{center}
    \LARGE{\textbf{\moduleno{}: \moduletitle{}}}

    \Large{Week \weekno{}: \labtitle{}}

    \vspace{0.5cm}
    
    \includegraphics[height=4cm]{img/Java-Emblem.jpg}
    
    \vspace{0.5cm}
\end{center}

This session explores event-driven programming and multi-threading in Java. These concepts are used to add interactivity to applications and improve efficiency. By the end of this lab, you'll be well-versed in creating responsive Java applications.

\section{Basic event handling}

Events are actions or occurrences that take place during program execution, it can be triggered by the user through actions such as button clicks, mouse movements, and keyboard inputs. Event handling is an important aspect in Java, particularly used in graphical user interface (GUI) programming. GUI libraries such as Abstract Window Toolkit (AWT) and Swing are typically used to facilitate event handling processes. Event handling can be broken down into three primary components: event sources, event listeners, and event classes. 

An event source is responsible for generating events during program execution. In GUI programming, components such as buttons, text fields, and other interactive elements are event sources. In a typical event handling sequence, event listeners are assigned to event sources to define how the program should responds to an event. Separating event generation and handling allows code to be modularised. 
The Swing library can be used to create interactive elements which serves as an event source. Consider the following example where a simple button is being implemented: 

\begin{minted}[frame=single,framesep=4pt, fontfamily=tt, escapeinside=||, mathescape=true, linenos]{java}
import javax.swing.JButton; // Import libraries
import javax.swing.JFrame;

public class SimpleButtonExample {
    public static void main(String[] args) {
        // Create a JFrame (window)
        JFrame frame = new JFrame("Simple Button Example");

        // Create a JButton
        JButton button = new JButton("Click me!");

        // Add the button to the frame
        frame.getContentPane().add(button);

        // Set frame properties
        frame.setSize(300, 200);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setVisible(true);
    }
}
\end{minted}

Currently, clicking the button does not result in any action, as the event source has not been associated with an event listener. 

Event listeners are designed to respond to specific types of events. Essentially, they are specialised objects which receive notifications when events occur and can be used to define the behaviour that should be executed in response to these events.

In Java, events are encapsulated by dedicated event classes for a specific type of occurrence. These classes contain relevant information about the event, allowing developers to extract details such as the source of the event and any associated data. For instance, an \texttt{ActionListener} is notified when a button is clicked, enabling the developer to specify the actions to be taken at that moment. \texttt{MouseListener} is another interface responding to a change in the state of the mouse. The code above can be adapted to perform a simple function when the button is clicked:

\begin{minted}[frame=single,framesep=4pt, fontfamily=tt, escapeinside=||, mathescape=true, linenos]{java}
import javax.swing.JButton;
import javax.swing.JFrame;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class SimpleButtonExample {
    public static void main(String[] args) {
        // Create a JFrame (window)
        JFrame frame = new JFrame("Simple Button Example");

        // Create a JButton
        JButton button = new JButton("Click me!");

        // Add an ActionListener to the button
        button.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                // Code to be executed when the button is clicked
                System.out.println("Button clicked!");
            }
        });

        // Add the button to the frame
        frame.getContentPane().add(button);

        // Set frame properties
        frame.setSize(300, 200);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setVisible(true);
    }
}

\end{minted}

\subsection{ActionListener}
This section looks at using \texttt{ActionListener} to develop some simple programs.

\begin{question}
    Complete the code below to create a basic counter that displays the number of button clicks:
\end{question}

\begin{minted}[frame=single,framesep=4pt, fontfamily=tt, escapeinside=||, mathescape=true, linenos]{java}
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class ClickCounterApp {

    private static int clickCount = 0;

    public static void main(String[] args) {
        // TODO: Create JFrame
        // TODO: Create JButton
        // TODO: Create JLabel

        // Add ActionListener to the button
        button.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                // TODO: Increment the click count and update the label
            }
        });

        // TODO: Create JPanel


        // TODO: Add button and label to the panel
        // TODO: Add panel to the content pane of the frame

        // TODO: Set default close operation and size for the frame
        // TODO: Make the frame visible
    }

    // Function to increment the click count
    private static void incrementClickCount() {
        // TODO: Increment the click count
    }

    // Function to update the label with the current click count
    private static void updateLabel(JLabel label) {
        // TODO: Update the label with the current click count
    }
}

\end{minted}

\begin{question}
    The next program is to build an interface for a shopping app. 

    Please adhere to the following guidelines for the app interface:
    \begin{itemize}
    \item Create a main window for the application.
    \item Create a product panel to hold the product buttons.
    \item Set up a grid layout for the product panel to arrange buttons in a grid (3 rows, 1 column).
    \item Iterate through the array of products and create a button with the product name.
    \item Print a message indicating that the product has been added to the cart when button is clicked.
    \end{itemize}

    \vspace{3cm}
    Complete the following code:
\end{question}

\begin{minted}[frame=single,framesep=4pt, fontfamily=tt, escapeinside=||, mathescape=true, linenos]{java}
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class SimpleProductApp {
    public static void main(String[] args) {
        // TODO: Create the main frame
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        // TODO: Create a panel to hold the product buttons
        // TODO: Use a grid layout for three buttons

        // Create an array of Product objects
        Product[] products = {
                new Product("Product 1"),
                new Product("Product 2"),
                new Product("Product 3")
        };

        // Iterate through the products and create a button for each
        for (Product product : products) {
            // TODO: Create a button with the product name (use the Product class)

            // Add an ActionListener to the button
            button.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    // TODO: Print "[product name] has been added to cart"
                }
            });

            // TODO: Add the button to the productPanel
        }

        // Set the layout of the main frame and add the productPanel
        frame.setLayout(new BorderLayout());
        frame.add(productPanel, BorderLayout.CENTER);

        // Set the size and make the frame visible
        frame.setSize(300, 150);
        frame.setVisible(true);
    }

    // A simple class representing a product with a name
    static class Product {
        private String name;

        public Product(String name) {
            this.name = name;
        }

        public String getName() {
            return name;
        }
    }
}
\end{minted}

\subsection{MouseListener}
The MouseListener interface provides methods that can be implemented to respond to different mouse-related actions, such as clicks, movements, and entering/exiting a component.

Some key methods in the MouseListener interface includes: \begin{itemize}
    \item \textbf{\texttt{void mouseClicked(MouseEvent e)}}: Invoked when the mouse button is clicked (pressed and released) inside a component.
    \item \textbf{\texttt{void mousePressed(MouseEvent e)}}: Invoked when a mouse button is pressed (pressed but not released).
    \item \textbf{\texttt{void mouseReleased(MouseEvent e)}}: Invoked when a mouse button is released.
    \item \textbf{\texttt{void mouseEntered(MouseEvent e)}}: Invoked when the mouse enters a component.
    \item \textbf{\texttt{void mouseExited(MouseEvent e)}}: Invoked when the mouse exits a component.
\end{itemize}

Consider the following code which prints the mouse action:

\begin{minted}[frame=single,framesep=4pt, fontfamily=tt, escapeinside=||, mathescape=true, linenos]{java}
import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

public class MouseListenerExample {

    public static void main(String[] args) {
        JFrame frame = new JFrame("Mouse Listener Example");
        JPanel panel = new JPanel();

        panel.addMouseListener(new MouseListener() {
            @Override
            public void mouseClicked(MouseEvent e) {
                System.out.println("Mouse Clicked");
            }

            @Override
            public void mousePressed(MouseEvent e) {
                System.out.println("Mouse Pressed");
            }

            @Override
            public void mouseReleased(MouseEvent e) {
                System.out.println("Mouse Released");
            }

            @Override
            public void mouseEntered(MouseEvent e) {
                System.out.println("Mouse Entered");
            }

            @Override
            public void mouseExited(MouseEvent e) {
                System.out.println("Mouse Exited");
            }
        });

        frame.getContentPane().add(panel);
        frame.setSize(300, 200);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setVisible(true);
    }
}
\end{minted}

\begin{question}
    Modify your code from exercise 2 to replace the \texttt{ActionListener} with a \texttt{MouseListener}. The \texttt{mouseClicked} method must print the message indicating that the corresponding product has been added to the cart.
\end{question}

\section{Multi-threading}

Multithreading in Java allows concurrent execution of multiple threads, allowing efficient usage of system resources and improved application performance. A thread can be defined as a lightweight, independent unit of execution within a process. There are several key concepts related to multi-threading in Java, this section will focus on thread creation.

\subsection{Thread creation}

There are two main ways to create threads: by extending the \texttt{Thread} class or by implementing the \texttt{Runnable} interface.

You can create a new thread by extending the \texttt{Thread} class and overriding its \texttt{run()} method.

The \texttt{run()} method contains the code that will be executed in the new thread:

\begin{minted}[frame=single,framesep=4pt, fontfamily=tt, escapeinside=||, mathescape=true, linenos]{java}
class MyThread extends Thread {
    public void run() {
        // Code to be executed in the new thread
    }
}
\end{minted}

To use this thread, create an instance of it and call the \texttt{start()} method.

\begin{minted}[frame=single,framesep=4pt, fontfamily=tt, escapeinside=||, mathescape=true, linenos]{java}
MyThread myThread = new MyThread();
myThread.start();
\end{minted}

Alternatively, you can implement the \texttt{Runnable} interface. 

\begin{minted}[frame=single,framesep=4pt, fontfamily=tt, escapeinside=||, mathescape=true, linenos]{java}
class MyRunnable implements Runnable {
    public void run() {
        // Code to be executed in the new thread
    }
}
\end{minted}

To use this thread, create an instance of it, pass it to a \texttt{Thread} constructor, and call the \texttt{start()} method.

\begin{minted}[frame=single,framesep=4pt, fontfamily=tt, escapeinside=||, mathescape=true, linenos]{java}
MyRunnable myRunnable = new MyRunnable();
Thread myThread = new Thread(myRunnable);
myThread.start();
\end{minted}

A simple example below uses the first approach:
\begin{minted}[frame=single,framesep=4pt, fontfamily=tt, escapeinside=||, mathescape=true, linenos]{java}
class MyThread extends Thread {
    public void run() {
        for (int i = 0; i < 5; i++) {
            System.out.println("Thread: " + i);
            try {
                Thread.sleep(500); // Simulating some work
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}

public class ThreadCreationExample {
    public static void main(String[] args) {
        MyThread myThread = new MyThread();
        myThread.start();

        // Main thread continues its work
        for (int i = 0; i < 5; i++) {
            System.out.println("Main: " + i);
            try {
                Thread.sleep(500); // Simulating some work
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
\end{minted}

\section{Advanced event handling}

Advanced event handling becomes essential in more complex programs due to the need for sophisticated interactions and responsiveness. Basic event handling techniques, explored in the first section, may fall short when dealing with diverse user inputs, simultaneous actions, and complex scenarios. 

Advanced event handling techniques, such as custom events and listeners for multiple components, allow developers to design more flexible and extensible systems. Custom events enable the creation of tailored communication between components, facilitating a finer level of control in response to user actions.

\subsection{Custom events}

To create custom events, typically the following steps are used: define a class that extends java.util.EventObject to represent the event, create an interface extending java.util.EventListener with methods for handling the event, and incorporate an EventListenerList in the class generating events. This framework facilitates the registration of listeners, triggering events, and notifying registered listeners. 

Refer to the following code that defines a simple custom event-handling mechanism:

\begin{minted}[frame=single,framesep=4pt, fontfamily=tt, escapeinside=||, mathescape=true, linenos]{java}
import java.util.EventListener;
import java.util.EventObject;
import javax.swing.event.EventListenerList;

// Custom event class representing a MyEvent occurrence
class MyEvent extends EventObject {
    public MyEvent(Object source) {
        super(source);
    }
}

// Interface for the MyEventListener, extending EventListener
interface MyEventListener extends EventListener {
    // Method to be implemented by classes listening for MyEvent occurrences
    void myEventOccurred(MyEvent evt);
}

// Class providing the framework for custom event handling
class MyClass {
    // EventListenerList to manage registered listeners
    protected EventListenerList listenerList = new EventListenerList();

    // Method to add a MyEventListener to the list of listeners
    public void addMyEventListener(MyEventListener listener) {
        listenerList.add(MyEventListener.class, listener);
    }

    // Method to remove a MyEventListener from the list of listeners
    public void removeMyEventListener(MyEventListener listener) {
        listenerList.remove(MyEventListener.class, listener);
    }

    // Method to trigger the MyEvent and notify registered listeners
    void fireMyEvent(MyEvent evt) {
        // Get the array of listeners
        Object[] listeners = listenerList.getListenerList();

        // Iterate over the array (every second element is a class object)
        for (int i = 0; i < listeners.length; i = i + 2) {
            // Check if the current element is of type MyEventListener
            if (listeners[i] == MyEventListener.class) {
                // Invoke the myEventOccurred method on the listener
                ((MyEventListener) listeners[i + 1]).myEventOccurred(evt);
            }
        }
    }
}

// Main class to demonstrate the usage of the event handling mechanism
public class Main {
    public static void main(String[] argv) throws Exception {
        // Create an instance of MyClass
        MyClass c = new MyClass();

        // Add a MyEventListener to respond to MyEvent occurrences
        c.addMyEventListener(new MyEventListener() {
            // Implementation of the myEventOccurred method
            public void myEventOccurred(MyEvent evt) {
                System.out.println("MyEvent occurred: fired");
            }
        });
    }
}
\end{minted}

\vspace{1cm}

\begin{question}
    Using your understanding of custom events, enhance the shopping app by incorporating a custom event handler. 

    Complete the following code:
\end{question}

\begin{minted}[frame=single,framesep=4pt, fontsize=\scriptsize, fontfamily=tt, escapeinside=||, mathescape=true, linenos]{java}
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class SimpleProductApp {
    public static void main(String[] args) {
        // TODO: Create the main frame
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        // TODO: Create a panel to hold the product buttons
        // TODO: Use a grid layout for three buttons

        // Create an array of Product objects
        Product[] products = {
                new Product("Product 1"),
                new Product("Product 2"),
                new Product("Product 3")
        };

        // Create custom event and listener
        ProductSelectionEventDispatcher eventDispatcher = new ProductSelectionEventDispatcher();

        ProductSelectionListener selectionListener = new ProductSelectionListener() {
            @Override
            public void productSelected(Product selectedProduct) {
                // TODO: Print "[product name] has been added to cart"
            }
        };

        // TODO: Add the listener to the dispatcher

        // Iterate through the products and create a button for each
        for (Product product : products) {
            // TODO: Create a button with the product name

            // Add an ActionListener to the button
            button.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    // Notify the event dispatcher when a button is clicked
                    eventDispatcher.fireProductSelectionEvent(product);
                }
            });

            // TODO: Add the button to the productPanel
        }

        // Set the layout of the main frame and add the productPanel
        frame.setLayout(new BorderLayout());
        frame.add(productPanel, BorderLayout.CENTER);

        // Set the size and make the frame visible
        frame.setSize(300, 150);
        frame.setVisible(true);
    }

    // A simple class representing a product with a name
    static class Product {
        private String name;

        public Product(String name) {
            this.name = name;
        }

        public String getName() {
            return name;
        }
    }

    // TODO: Create custom event class
    static class ProductSelectionEvent extends EventObject {
        
    }

    // TODO: Create custom listener interface
    interface ProductSelectionListener extends EventListener {
		
    }

    // Custom event dispatcher
    static class ProductSelectionEventDispatcher {
        // EventListenerList to manage registered listeners
        private final EventListenerList listenerList = new EventListenerList();

        // TODO: Create method to add ProductSelectionListener to the list of listeners
        public void addProductSelectionListener(ProductSelectionListener listener) {
            
        }

        // TODO: Create method to remove ProductSelectionListener from the list of listeners
        public void removeProductSelectionListener(ProductSelectionListener listener) {
            
        }

        public void fireProductSelectionEvent(Product selectedProduct) {
            // TODO: Get the list of listeners

            // TODO: Create a new event object

            // TODO: Iterate over the listeners and notify them about the event
                // TODO: Check if the current element is of type ProductSelectionListener
                    // TODO: Call the appropriate method in the listener
                }
            }
        }
    }
}

\end{minted}

\subsection{Event listeners for multiple components}

Event handlers for multiple components enable a streamlined approach to responding to events generated by various GUI elements. Often a centralised event handler is created that can handle events from diverse sources (e.g., \texttt{ActionListener} for button clicks or \texttt{MouseListener} for mouse events). Additionally, this approach allows the event-handling logic to be more efficient and the codebase to be more organised.

Consider the example below which creates a button and checkbox. Both components share a common ActionListener that handles their events. When the button is clicked, it prints "Button Clicked!" and when the checkbox state changes it prints the updated state:

\begin{minted}[frame=single,framesep=4pt, fontsize=\scriptsize, fontfamily=tt, escapeinside=||, mathescape=true, linenos]{java}

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class MultipleComponentsExample {
    public static void main(String[] args) {
        JFrame frame = new JFrame("Multiple Components Example");

        JButton button = new JButton("Click Me");
        JCheckBox checkBox = new JCheckBox("Check Me");

        // Common ActionListener for both components
        ActionListener commonActionListener = new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (e.getSource() == button) {
                    System.out.println("Button Clicked!");
                } else if (e.getSource() == checkBox) {
                    System.out.println("Checkbox State Changed: " + checkBox.isSelected());
                }
            }
        };

        button.addActionListener(commonActionListener);
        checkBox.addActionListener(commonActionListener);

        frame.setLayout(new BoxLayout(frame.getContentPane(), BoxLayout.Y_AXIS));
        frame.add(button);
        frame.add(checkBox);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setLocationRelativeTo(null);
        frame.setVisible(true);
    }
}
\end{minted}

\begin{question}
    By combining your understanding of custom events and event listeners for multiple components, create a more realistic shopping app simulation. You may wish to use the following classes to create the app:

    \begin{itemize}
    \item \textbf{\texttt{Product}} represents a product that can be purchased, with a name and price
    \item \textbf{\texttt{ShoppingCart}} keeps track of the products added to the cart and the total price
    \item \textbf{\texttt{ProductButton}} is a custom button for each product
    \item \textbf{\texttt{AddToCartEvent}} is a custom event triggered each time a product is added to the cart
    \item \textbf{\texttt{AddToCartListener}} is an interface for classes which listen for \texttt{AddToCartEvent}s
    \item \textbf{\texttt{ShoppingCartPanel}} displays the total price and listens for \texttt{AddToCartEvent}s
    \item \textbf{\texttt{ProductPanel}} displays a grid of \texttt{ProductButton}s
    \item \textbf{\texttt{ProductMouseListener}} handles mouse clicks on \texttt{ProductButton}s and triggers an \texttt{AddToCartEvent} each time a button is clicked
    \item \textbf{\texttt{EventBus}} is a simple event bus for managing listeners and firing events
    \end{itemize}

    Complete the following code:
\end{question}

\begin{minted}[frame=single,framesep=4pt, fontsize=\scriptsize, fontfamily=tt, escapeinside=||, mathescape=true, linenos]{java}
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

// TODO: Create a product class with name and price
class Product {
    private String name;
    private double price;

}

// Represents a shopping cart that keeps track of the total price of added products
class ShoppingCart {
    private double total;

    // Adds a product to the shopping cart
    public void addToCart(Product product) {
        total += product.getPrice();
    }

    // TODO: Retrieve the total price in the shopping cart
    public double getTotal() {

    }
}

// Represents a button associated with a product
class ProductButton extends JButton {
    private Product product;

    public ProductButton(Product product) {
        super(product.getName());
        this.product = product;
    }

    // Retrieves the associated product
    public Product getProduct() {
        return product;
    }
}

// TODO: Create custom event class indicating that a product has been added to the cart
class AddToCartEvent extends java.util.EventObject {
    
}

// TODO: Create interface for listeners interested in the AddToCartEvent
interface AddToCartListener extends java.util.EventListener {

}

// Represents a panel displaying the shopping cart and acting as an AddToCartListener
class ShoppingCartPanel extends JPanel implements AddToCartListener {
    private ShoppingCart shoppingCart;
    private JLabel totalLabel;

    public ShoppingCartPanel() {
        shoppingCart = new ShoppingCart();
        totalLabel = new JLabel("Total: £0.0");

        setLayout(new BorderLayout());
        add(totalLabel, BorderLayout.NORTH);

        // TODO: Register as a listener for AddToCartEvents
    }

    // Called when an AddToCartEvent occurs
    @Override
    public void addToCartOccurred(AddToCartEvent event) {
        Product product = (Product) event.getSource();
        shoppingCart.addToCart(product);
        updateTotalLabel();
    }

    // TODO: Update the totalLabel with the current total from the shopping cart
    private void updateTotalLabel() {

    }
}

// Represents a panel displaying product buttons
class ProductPanel extends JPanel {
    public ProductPanel() {
        setLayout(new GridLayout(3, 2));

        // Create an array of products
        Product[] products = {
                new Product("Product 1", 10.0),
                new Product("Product 2", 20.0),
                new Product("Product 3", 30.0)
        };

        // TODO: Create buttons for each product and add them to the panel
        for (Product product : products) {
            
        }
    }
}

// Represents a mouse listener for the product buttons
class ProductMouseListener extends MouseAdapter {
    @Override
    public void mouseClicked(MouseEvent e) {
        if (e.getSource() instanceof ProductButton) {
            ProductButton button = (ProductButton) e.getSource();
            Product product = button.getProduct();

            // Dispatch AddToCartEvent to all listeners
            EventBus.getInstance().fireAddToCartEvent(new AddToCartEvent(product));
        }
    }
}

// Represents a simple event bus for communication between components
class EventBus {
    private static EventBus instance;
    private java.util.List<AddToCartListener> listeners;

    private EventBus() {
        listeners = new java.util.ArrayList<>();
    }

    // Ensures only one instance of EventBus
    public static EventBus getInstance() {
        if (instance == null) {
            instance = new EventBus();
        }
        return instance;
    }

    // Adds a listener to the list
    public void addAddToCartListener(AddToCartListener listener) {
        listeners.add(listener);
    }

    // Notifies all listeners of an AddToCartEvent
    public void fireAddToCartEvent(AddToCartEvent event) {
        for (AddToCartListener listener : listeners) {
            listener.addToCartOccurred(event);
        }
    }
}

// Main class for the shopping app
public class ShoppingApp {
    public static void main(String[] args) {
        JFrame frame = new JFrame("Shopping App");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        // TODO: Create product and shopping cart panels
        // TODO: Set up the layout of the main frame
        // TODO: Set the size and make the frame visible
    }
}
\end{minted}

\section{Concurrent event handling} 

The combination of multithreading and event handling is essential for developing responsive and efficient programs. Multithreading allows concurrent task execution, preventing the main thread from being blocked by time-consuming operations. When integrated with event handling, this approach ensures that user interfaces remain responsive to user interactions. The above sections have been aimed at imparting knowledge about event programming and multithreading. These concepts must now be combined to create an efficient and realistic user interface.

\begin{question}
    Research thread pooling in Java as an extension to the concept of thread creation.
\end{question}

Consider the following code which defines an \texttt{Order} class with order details. The \texttt{OrderProcessor} class simulates order processing, including cost calculation, inventory update, and order confirmation. The main class generates an array of orders and submits corresponding processing tasks to the thread pool. It collects the results using \texttt{Future} objects, waits for the tasks to complete, and prints the order processing details. Finally, it shuts down the thread pool. This is an example of thread pooling in action.

\begin{minted}
[frame=single,framesep=4pt, fontsize=\scriptsize, fontfamily=tt, escapeinside=||, mathescape=true, linenos]{java}
import java.util.concurrent.*;

class Order {
    private int orderId;
    private String product;
    private int quantity;

    public Order(int orderId, String product, int quantity) {
        this.orderId = orderId;
        this.product = product;
        this.quantity = quantity;
    }

    public int getOrderId() {
        return orderId;
    }

    public String getProduct() {
        return product;
    }

    public int getQuantity() {
        return quantity;
    }
}

class OrderProcessor implements Callable<String> {
    private Order order;

    public OrderProcessor(Order order) {
        this.order = order;
    }

    @Override
    public String call() throws Exception {
        // Simulating order processing
        Thread.sleep(2000);

        // Perform order processing actions
        int totalCost = calculateTotalCost();
        updateInventory();
        sendOrderConfirmation();

        return "Order processed - OrderID: " + order.getOrderId() +
               ", Product: " + order.getProduct() +
               ", Quantity: " + order.getQuantity() +
               ", Total Cost: £" + totalCost;
    }

    private int calculateTotalCost() {
        // Simulating cost calculation based on product and quantity
        return order.getQuantity() * 10; // Assuming £10 per unit
    }

    private void updateInventory() {
        // Simulating inventory update
        System.out.println("Inventory updated for " + order.getProduct());
    }

    private void sendOrderConfirmation() {
        // Simulating sending order confirmation
        System.out.println("Order confirmation sent for OrderID: " + order.getOrderId());
    }
}

public class ConcurrentOrderProcessingExample {

    public static void main(String[] args) {
        ExecutorService executorService = Executors.newFixedThreadPool(3);

        try {
            // Generate a list of orders
            Order[] orders = {
                    new Order(1, "Laptop", 2),
                    new Order(2, "Smartphone", 3),
                    new Order(3, "Headphones", 5),
                    new Order(4, "Tablet", 1),
                    new Order(5, "Camera", 2)
            };

            // Create a list to hold Future objects
            List<Future<String>> futures = new ArrayList<>();

            // Submit tasks to the executor and store the Future objects
            for (Order order : orders) {
                OrderProcessor processor = new OrderProcessor(order);
                Future<String> future = executorService.submit(processor);
                futures.add(future);
            }

            // Wait for all tasks to complete and retrieve the results
            for (Future<String> future : futures) {
                try {
                    String result = future.get();
                    System.out.println(result);
                } catch (InterruptedException | ExecutionException e) {
                    e.printStackTrace();
                }
            }
        } finally {
            // Shutdown the executor service
            executorService.shutdown();
        }
    }
}
\end{minted}

\begin{question}
    As an extension task, consider planning and implementing thread pooling to further enhance the shopping app simulation.
\end{question}

\end{document}




