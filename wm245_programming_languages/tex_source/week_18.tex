\documentclass[11pt,oneside,a4paper]{article}
\usepackage[%
	a4paper,%
	left = 20mm,%
	right = 20mm,%
	textwidth = 178mm,%
	top = 35mm,%
	bottom = 30mm,%
	headheight=70pt,%
headsep=25pt,%
]{geometry}
\usepackage{graphicx}
\usepackage[inkscapeformat=pdf]{svg}
\usepackage{transparent}
\usepackage{lastpage}

\usepackage{fontspec}
\setmainfont{Montserrat}
\setmainfont{Noto Sans}

\input{helper/preamble.tex}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Edit below
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\def\weekno{18}
\def\labtitle{Rust Up!}
%\def\labtitle{Rust to Infinity and Beyond}
\input{module_info}

\setmainfont[
    UprightFont = {Montserrat Light},
    BoldFont = {Montserrat Bold},
    ItalicFont = {Montserrat Italic},
    BoldItalicFont = {Montserrat Bold Italic}
]{Montserrat}

\input{helper/header.tex}

\title{Week \weekno{}: \labtitle{}}
\author{\moduleno{} \moduletitle{}}
\date{January 2024}

\begin{document}

\begin{center}
    \LARGE{\textbf{\moduleno{}: \moduletitle{}}}

    \Large{Week \weekno{}: \labtitle{}}

    \vspace{0.5cm}

    \includegraphics[height=4cm]{img/embedded_ferris.png}

    \vspace{0.5cm}
\end{center}

This is a continuation of the Introduction to Rust lab, and will introduce you to some of the features that make Rust unique.

\section{Understanding ownership}

We noted earlier that Rust has an emphasis on speed and memory safety. One of the most important ways in which the compiler enforces these qualities is by implementing a strict model of ownership, a set of rules which dictate how data can be bound to variables over the lifetime of a program. These rules are checked by the language at compile time, meaning they dramatically improve code safety while having next to no impact on runtime performance.

Because ownership is a new concept for many programmers, it does take a while to get your head around. You may find yourself banging your head from time to time when the language refuses to compile programs which you believe \emph{ought to} be safe, but which violate the ownership model. However, as you get used to programming in Rust and understanding the rules of the ownership system, you'll begin to find it easier to naturally write safe, efficient code!
\headerspace{}

\subsection{The stack and the heap}

Before discussing how ownership works, it's important to remind ourselves how programming languages manage memory. This is done using two regions of memory, the stack and the heap.

The stack is where all variables local to each function call are stored. When a function is called, a stack frame is constructed on top of the stack and all function arguments and local variables are stored in it. Then, when the function returns or otherwise goes out of scope, its stack frame gets taken down and all data in it is cleared. This evidently makes the stack unsuitable when you want to use a function to create data structures and return pointers or references to them -- as soon as you try to return the reference, the object goes out of scope, making the reference no longer valid.

To prevent data from being removed during program execution, objects which need to be passed around are often stored in the heap. The heap is function-agnostic and remains in place during the lifetime of the program's execution. However, it is also relatively unstructured: to put data on the heap, the program needs to request space from the memory allocator, which searches for an empty spot in the heap that is big enough to store the data needed. Furthermore, because data on the heap is not removed automatically, the programming language needs a way to deallocate memory once it's no longer needed to prevent memory leaks.

There are two main approaches in modern programming languages for deciding what gets placed on the stack and what gets placed in the heap. Some languages, such as Java and Python, place all primitive datatypes (such as integers, chars and Booleans) on the stack and all objects on the heap. Others, such as C and C++, place all data on the stack unless the programmer declares that they should be on the heap (using a keyword such as \texttt{malloc} or \texttt{new}). Rust follows the approach of the C family: with a few exceptions, all objects are placed on the stack unless they are boxed using the \texttt{Box} type.

Another important aspect of memory management is how programming languages deallocate memory from the heap when it's no longer needed. There are a couple of ways in which different languages deallocate memory:

\begin{itemize}
    \item By default, C++ uses scope-bound resource management\footnote{Also known by the exceptionally hideous name \emph{resource acquisition is initialisation} (RAII).}, where all objects are bound to a variable and are destroyed when that variable goes out of scope
    \item Java, Python and Go use garbage collection, where a garbage collector periodically erases data that is no longer bound to a variable at runtime
    \item C and C++ (when using \texttt{new}) require the programmer to explicitly free memory in the program's code, and leak memory when this is not done
\end{itemize}

Rust uses ownership as its solution for allocating and deallocating memory. It is similar to scope-based resource management, with additional safety guarantees.
\headerspace{}

\subsection{Motivation}

To illustrate why ownership is useful, consider the code snippet below, which creates a vector and assigns it to both \texttt{v1} and \texttt{v2}.

\begin{minted}[frame=single,framesep=4pt, fontfamily=tt, escapeinside=||, mathescape=true, linenos]{rust}
fn main() {
    let v1 = vec![1, 2, 3];
    let v2 = v1;
}
\end{minted}

A vector is a collection of data values similar to a list in other languages. While it has a fixed type, it does not have a fixed size, and you are free to add and remove elements from it at will. In Rust, vector objects live on the stack, while their data (the elements they contain) are placed in the heap. This nuance is significant as it means that a vector's metadata and its data are stored in different parts of memory.

In the above example, line 2 pushes a vector object onto the stack and allocates its data \texttt{[1, 2, 3]} (on the heap). A reference to the data is then copied to the vector and becomes part of the object.

When we copy \texttt{v1} to \texttt{v2} in line 3, Rust performs a bitwise copy of \texttt{v1}. Note that this does not copy the underlying data -- \texttt{v2} has the same data pointer that \texttt{v1} had. Essentially, we have copied a reference to \texttt{v1}.

Now that \texttt{v1} and \texttt{v2} have the same data but different metadata, using either of the variables causes the variables to become inconsistent. For example, if we truncate \texttt{v2} to two elements, the the memory allocator will free the third element's space on the heap, but \texttt{v1} won't know that the heap space was cropped. Its length will still be set to 3, and it try to let us access \texttt{v1[2]}, creating a use-after-free vulnerability.

\begin{minted}[frame=single,framesep=4pt, fontfamily=tt, escapeinside=||, mathescape=true, linenos]{rust}
fn main() {
    let v1 = vec![1, 2, 3];
    let v2 = v1;

    v2.truncate(2);
    v1[2];           // Oh no!
}
\end{minted}

To protect against this, Rust allows the vector to be only bound to (or ``owned by'') a single variable at a time. In line 2 this is \texttt{v1}, but, when \texttt{v1} is assigned to \texttt{v2}, ownership is transferred to \texttt{v2}. Once ownership is transferred, \texttt{v1} is no longer valid, and our program won't compile if we later try to use \texttt{v1} in any way:

\begin{minted}[frame=single,framesep=4pt, fontfamily=tt, escapeinside=||, mathescape=true, linenos]{rust}
fn main() {
    let v1 = vec![1, 2, 3];
    let v2 = v1;

    println!("v1[0] is: {v1[0]}");
}
\end{minted}

Try compiling the code example above and see what error you get!
\newpage

\subsection{The Rust ownership model}

We can see from the example above that we can improve memory safety by ensuring that each object in memory can only be accessed by a single variable or reference at a time. This is what we mean by \emph{ownership}: each object\footnote{The exception is types which implement the \texttt{Copy} trait, such as primitives.} is bound to a variable or reference which \emph{owns} it, and the owner is the only variable which can use or manage the object.

The Rust two ownership model has three main rules: \begin{enumerate}
    \item Each value in Rust has an owner
    \item Ownership can be transferred, but each value can only have a single owner at a time
    \item A value is dropped when its owner goes out of scope
\end{enumerate}

We saw above that preventing objects from having multiple references means we no longer have to worry about inconsistency between references to the same object. Other memory errors and vulnerabilities that are resolved by Rust's ownership model include dangling references, double frees, segmentation faults and memory leaks.
\headerspace{}

\subsection{Ownership and functions}

When a function is passed an argument, it becomes the new owner of that value. If we want to use that value again afterwards, we must return ownership of the value by returning and reassigning it:

\begin{minted}[frame=single,framesep=4pt, fontfamily=tt, escapeinside=||, mathescape=true, linenos]{rust}
fn main() {
    let mut story = String::from("Once upon a time, there lived...");
    story = tell_story(story);

    println!("main : {story}");
}

fn tell_story(s: String) -> String {
    println!("f    : {s}");
    s
}
\end{minted}

A function is considered to be ``using'' an object as soon as it is called with that object as an argument, so the function doesn't have to use the value in its definition to take ownership. For example, the following code snippet doesn't compile:
\newpage

\begin{minted}[frame=single,framesep=4pt, fontfamily=tt, escapeinside=||, mathescape=true, linenos]{rust}
fn main() {
    let s = String::from("hello");
    do_nothing(s);

    println!("{s}");
}

fn do_nothing(s: String) { }
\end{minted}

Of course, this doesn't happen with \texttt{Copy} objects, so the following code snippet compiles:

\begin{minted}[frame=single,framesep=4pt, fontfamily=tt, escapeinside=||, mathescape=true, linenos]{rust}
fn main() {
    let b = true;

    let _y = change_truth(b);
    println!(b);
}

fn change_truth(x: bool) -> bool {
    !x
}
\end{minted}

However, if we have to frequently return ownership from functions, our code can start to look messy. Fortunately, Rust has one more neat little feature to prevent this: borrowing.
\headerspace{}

\subsection{Borrowing}

If we intend a function to return ownership of any values passed, we can ask it to \emph{borrow} the value instead of using it. Just like in real life, when you borrow something, you don't own it, and you have to give it back when you're done using it.

We make a function borrow a value by passing a reference to it instead of the value itself:

\begin{minted}[frame=single,framesep=4pt, fontfamily=tt, escapeinside=||, mathescape=true, linenos]{rust}
fn main() {
    let mut story = String::from("Once upon a time, there lived...");
    story = tell_story(&story);

    println!("main : {story}");
}

fn tell_story(&s: String) {
    println!("f    : {s}")
}
\end{minted}

By default, references aren't mutable. To modify borrowed values, you must pass a mutable reference using \texttt{\&mut}:

\begin{minted}[frame=single,framesep=4pt, fontfamily=tt, escapeinside=||, mathescape=true, linenos]{rust}
fn main() {
    let mut s = String::from("hello");

    change(&mut s);
    println!(s);
}

fn change(some_string: &mut String) {
    some_string.push_str(", world");
}
\end{minted}

It can be tricky deciding whether a function should borrow or use its arguments. In general, you will likely find it preferable for functions to borrow values rather than using them, simply because borrowing allows you to continue using the value afterwards. Taking ownership of arguments generally makes more sense when the function is designed to consume the value in some way.
\headerspace{}

\subsection{Cloning}

Rust also lets us \emph{clone} values, but cloning should be performed limitingly and care should be taken when doing so.

\begin{minted}[frame=single,framesep=4pt, fontfamily=tt, escapeinside=||, mathescape=true, linenos]{rust}
// A tuple struct with resources that implements the `Clone` trait
#[derive(Clone, Debug)]
struct Pair(Box<i32>, Box<i32>);

fn main() {
    let pair = Pair(Box::new(1), Box::new(2));
    println!("Original pair: {:?}", pair);

    let cloned_pair = moved_pair.clone();
    drop(pair);                                  // Deallocate pair

    println!("Cloned pair: {:?}", cloned_pair);  // We can still print cloned_pair
}
\end{minted}
\newpage

\subsection{Lifetimes}

Borrowing is simple when we know that the object we're borrowing won't expire, but what if a borrowed object might be dropped before we're done using it? If we continued using the reference, that would cause a use-after-free, which is bad. Fortunately, Rust allows us to annotate borrowed values with lifetimes to prevent this.

To give an example of the problem, consider the code snippet below:

\begin{minted}[frame=single,framesep=4pt, fontfamily=tt, escapeinside=||, mathescape=true, linenos]{rust}
fn main() {
    let r: &int;
    {
        let i = 1;      // Introduce scoped value i
        r = &i;         // Store a reference to i in r
    }                   // i goes out of scope and is dropped

    println!("{}", r);  // r still refers to i
}
\end{minted}

In the example above, the Rust compiler can tell that \texttt{r} is used after \texttt{i} expires, and so it refuses to compile the program. However, what if the lifetime of a value is ambiguous? For example, consider the function below:

\begin{minted}[frame=single,framesep=4pt, fontfamily=tt, escapeinside=||, mathescape=true, linenos]{rust}
fn add_prefix(s: &str, prefix: &str) -> &str {
    // ???
}

fn main() {
    let prefix    = "Dr";
    let firstname = "Emmett";
    let surname   = "Brown"

    let qualified_name;
    {
        let fullname = format!("{firstname} {surname}");  // fullname comes into scope
        qualified_name = add_prefix();
    }                                                     // fullname goes out of scope

    println!("Hello, {qualified_name}.");
}
\end{minted}

Here, whether line 16 is safe depends on whether the lifetime of \texttt{add\_prefix} depends on \texttt{\&prefix} (which is still alive) or \texttt{\&s} (which is dropped in line 13). This cannot be verified by checking the return type of \texttt{add\_prefix}, so the compiler refuses to compile the program. To get it to compile, we need to annotate the function with the lifetimes of the values it's borrowing. This is done as part of the function signature:

\begin{minted}[frame=single,framesep=4pt, fontfamily=tt, escapeinside=||, mathescape=true, linenos]{rust}
fn add_prefix<'a, 'b>(s: &'a str, prefix: &'b str) -> &'b str {
    // ...
}
\end{minted}

Let's examine the changes. We first added a generic \texttt{<'a, 'b>} after the method name, which introduces two lifetime parameters \texttt{'a} and \texttt{'b}. A lifetime tells the compiler how long it can expect a value to remain valid compared to another: we can tell it that the lifetime of a reference \texttt{\&x} can be the same as, different from, greater than or less than a reference \texttt{\&y}.

Next, we associated each reference in the function parameters with one of these lifetimes by changing the type of each reference. We can specify the same lifetime for multiple values, which tells the compiler that, as soon as any of them goes out of scope, all of them can be considered invalid. For example, in the following function:\footnote{\href{https://users.rust-lang.org/t/define-a-function-that-returns-either-of-its-arguments-with-different-lifetimes/25253/3}{Source}}

\begin{minted}[frame=single,framesep=4pt, fontfamily=tt, escapeinside=||, mathescape=true, linenos]{rust}
fn longest<'a>(x: &'a str, y: &'a str) -> &'a str {
    if x.len() > y.len() { x } else { y }
}
\end{minted}

The compiler is free to look for a point of termination \texttt{'a} which outlives \texttt{'x} and \texttt{'y} and isn't outlived by the return value.

Finally, we added a lifetime to the return type, telling the compiler that it should consider the return type invalid once \texttt{\&x} is dropped. As a result, the compiler can now tell that the return value of \texttt{add\_prefix} is safe to use even after \texttt{s} goes out of scope.

In simple and common cases (like what we've been learning so far), the compiler doesn't need these explicit lifetime annotations to be happy, and we can get away with implicit lifetime annotations (where we don't have to provide anything): this is known as \emph{lifetime elision}.

Finally, there exists a special lifetime \texttt{'static}, which specifies that a variable remains in scope for the entire lifetime of a program. In this sense, it is identical to the \texttt{static} keyword in other programming languages, but only references (and not values) can be made static. You have likely already implicitly seen static references before, in the case of strings (which are implicitly static).

\begin{minted}[frame=single,framesep=4pt, fontfamily=tt, escapeinside=||, mathescape=true, linenos]{rust}
fn main() {
    let s: &'static str = "Hello, world!";
}
\end{minted}
\newpage

\subsection{Exercises}

\begin{enumerate}
    \item In the example in Section 1.7, give an example of a definition for \texttt{add\_prefix} for which the lifetime of its result depends on \texttt{\&s}, and a definition for which the lifetime depends on \texttt{\&prefix}. Show that either definition will fail to compile if you don't provide its signature with lifetime annotations.
    \item Below are a number of function signatures. State whether each signature is valid from the compiler's point of view, giving a brief explanation for your answer.
\begin{minted}[frame=single,framesep=4pt, fontfamily=tt, escapeinside=||, mathescape=true, linenos]{rust}
fn substr(s: &str, until: u32) -> &str;
fn substr<'a>(s: &'a str, until: u32) -> &'a str;

fn get_str() -> &str;

fn frob(s: &str, t: &str) -> &str;
fn frob<'a, 'b>(s: &'a str, t: &'b str) -> &str;

fn get_mut(&mut self) -> &mut T;
fn get_mut<'a>(&'a mut self) -> &'a mut T;

fn args<T: ToCStr>(&mut self, args: &[T]) -> &mut Command;
fn args<'a, 'b, T: ToCStr>(&'a mut self, args: &'b [T]) -> &'a mut Command;

fn new(buf: &mut [u8]) -> BufWriter;
fn new<'a>(buf: &'a mut [u8]) -> BufWriter<'a>;
\end{minted}
    \item List as many types as you can find which implement the \texttt{Copy} trait.
    \item Write a function \texttt{fn bubble\_sort<T: PartialOrd>(v: \&mut Vec<T>)} which takes a reference to a vector \texttt{v} and sorts it in place.
    \item Modify your function so that \texttt{v} is an immutable reference, and \texttt{bubble\_sort} returns a new reference to a sorted vector, instead of sorting the vector in place.
    \item Modify your list so that it now returns the vector itself, instead of a reference to the vector. (\emph{Hint: what method do you need?})
    \newpage
    \item Take a look at the following program. Despite \texttt{tell\_story} taking ownership of \texttt{story}, the program compiles successfully. Why?
\begin{minted}[frame=single,framesep=4pt, fontfamily=tt, escapeinside=||, mathescape=true, linenos]{rust}
fn main() {
    let story = "Once upon a time, there lived...";
    tell_story(story.to_string());

    println!("main : {story}");
}

fn tell_story(s: String) {
    println!("f    : {s}");
}
\end{minted}
\end{enumerate}

\section{Error handling}

Like all programming languages, Rust needs a way to allow programs to recover gracefully when an error occurs.

Rust groups errors into two types: recoverable errors and unrecoverable errors. A recoverable error is an error which occurs when a function fails in some way, and which we wish to handle (such as by retrying the failed operation). Meanwhile, an unrecoverable error is an error which is symptomatic of bugs in a program, and so we wish to abort a program when it does occur. By using the two wisely, we can make our code more robust by ensuring that we discover and handle errors appropriately.
\headerspace{}

\subsection{Recoverable errors with \texttt{Result}}

Most errors aren't serious enough to require the program to stop entirely. For example, when a function fails, it's usually for a reason that you can easily interpret and respond to. Functions which expect to raise these errors typically report them using \texttt{Result}.

\texttt{Result} is an enum type with two possible values:

\begin{minted}[frame=single,framesep=4pt, fontfamily=tt, escapeinside=||, mathescape=true, linenos]{rust}
enum Result<T, E> {
    Ok(T),
    Err(E),
}
\end{minted}

Here, \texttt{T} and \texttt{E} are generic types, with \texttt{T} being the type of the value that is returned when the function completes successfully, and \texttt{E} being the type of the error that occurs when the function fails.
\newpage

As an example, let's consider opening a file.

\begin{minted}[frame=single,framesep=4pt, fontfamily=tt, escapeinside=||, mathescape=true, linenos]{rust}
use std::fs::File;

fn main() {
    let greeting_file_result = File::open("hello.txt");
}
\end{minted}

All going well, \texttt{File::open} returns a file handle for the file we wanted to open. However, \texttt{File::open} can also fail: for example, the file might not exist, or we might not have permission to open the file. This information is provided by the fact that \texttt{File::open} returns a \texttt{Result<File, io::Error>} object, rather than just a \texttt{File}.

Because we have a Result, we need to handle the two cases. There are a few ways of doing this, but the easiest way of doing this is using a \texttt{match} statement:

\begin{minted}[frame=single,framesep=4pt, fontfamily=tt, escapeinside=||, mathescape=true, linenos]{rust}
use std::fs::File;

fn main() {
    let greeting_file_result = File::open("hello.txt");

    let greeting_file = match greeting_file_result {
        Ok(file) => file,
        Err(error) => panic!("Problem opening the file: {:?}", error),
    };
}
\end{minted}

In the case where we want to propagate errors if the computation fails, the notation is so common that a shorthand operator exists for it, \texttt{?}.

\begin{minted}[frame=single,framesep=4pt, fontfamily=tt, escapeinside=||, mathescape=true, linenos]{rust}
use std::fs::File;
use std::io::{self, Read};

fn read_username_from_file() -> Result<String, io::Error> {
    let mut username_file = File::open("hello.txt")?;
    let mut username = String::new();
    username_file.read_to_string(&mut username)?;
    Ok(username)
}
\end{minted}

\texttt{?} assumes that the computation will succeed and attempts to extract its value. If the computation fails with type \texttt{Err(e)} \texttt{?} simply returns the same error. Hence, in the example above, \texttt{read\_username\_from\_file} will return a \texttt{Err(io::Error)} if the file can't be opened or read from.

\subsection{Unrecoverable errors with \texttt{panic!}}

Sometimes, bad things happen in your code and there's no way to recover from it. For cases like these, Rust has the \texttt{panic!} macro. When a program or thread panics, it prints a failure message and aborts the program.

\textbf{Note:} By default, when a program panics, the program starts unwinding. Rust walks back up the stack and cleans up the data from each stack frame it encoutners. However, this imposes a lot of overhead, so Rust allows you to immediately abort if the program panics (without cleaning up the program data) by adding \texttt{panic = 'abort'} to the appropriate \texttt{[profile]} sections of your Cargo.toml file.

\begin{minted}[frame=single,framesep=4pt, fontfamily=tt, escapeinside=||, mathescape=true, linenos]{rust}
[profile.release]
panic = 'abort'
\end{minted}

However, if you do this, the operating system becomes responsible for cleaning up the program data.

Try running the program below and see what happens!

\begin{minted}[frame=single,framesep=4pt, fontfamily=tt, escapeinside=||, mathescape=true, linenos]{rust}
fn main() {
    panic!("Crash and burn");
}
\end{minted}

In addition to unrecoverable errors, panics are often used to avoid undefined behaviour or memory access violations. For example, integer overflows (in debug environments), out-of-bound vector accesses, and assertion failures all cause the program to panic.

The \href{https://docs.rs/no-panic/latest/no_panic/}{\texttt{no\_panic}} and \href{https://crates.io/crates/no-panics-whatsoever}{\texttt{no-panics-whatsoever}} crates can be used to statically verify that a function will never panic. There are a few catches to be aware of when using this crate, so be sure to read the documentation first.

While it's possible to catch and handle panics using \texttt{catch\_unwind}, it's best to respect the philosophy of reserving panics for \emph{unrecoverable} errors and use \texttt{Result}s instead.

\begin{minted}[frame=single,framesep=4pt, fontfamily=tt, escapeinside=&&, mathescape=true, linenos]{rust}
use std::panic::catch_unwind;

fn main() {
    let v = vec![1, 2, 3];

    let panics = catch_unwind(|| v[99]).is_err();
    assert!(panics);

    println!("Hello, World");
}
\end{minted}
\newpage

% Good resource worth linking: https://opensource.com/article/20/5/rust-java

\subsection{Exercises}

\begin{enumerate}
    \item How does Rust handle integer overflows in debug environments? What about in release environments?
    \item Write a function \texttt{safe\_add(x, y)} which adds two integers together and panics if an integer overflow occurs.
    \item Write a function \texttt{safe\_divide(x, y)} which divides \texttt{x} by \texttt{y} and panics if either an overflow occurs or \texttt{y == 0}.
    \item Modify your functions so that, instead of panicking, they return a \texttt{Result} containing the answer. Comment on the advantages and disadvantages of each approach. Which would you choose to use? Justify your answer.
    \item Rust also provides the \texttt{unwrap} and \texttt{expect} methods for handling the \texttt{Result} type. Describe how they work, and explain the difference between the two.
    \item Write a custom data structure ArrayList which works with a list, doubling the list size when full.
    \item Write a function \texttt{read\_int\_from\_file(file: String)} that opens a file \texttt{file} containing a single integer, and returns the integer in the file.
    \item Write a CLI calculator program in Rust. Your program should read lines of input from \texttt{stdin}, and evaluate their results. Your program should support the \texttt{+}, \texttt{-}, \texttt{*} and \texttt{/} operations.
    \begin{enumerate}
        \item Extend your program so that users can omit the first argument (eg. by typing \texttt{+3}), in which case the most recently calculated result is used as the first argument. Your program should also support a \texttt{back} command to remove the most recently stored result from memory, and a \texttt{clear} command to clear the program's memory.
    \item Extend your program so that it also handles invalid inputs.
    \end{enumerate}
    \item The \href{https://app.codecrafters.io/tracks/rust}{Codecrafters Rust track} has a number of proposals for Rust projects. Try having a go at one or more of the projects!
\end{enumerate}
\newpage

\section{Further reading}

\begin{enumerate}
    \item \href{https://doc.rust-lang.org/book}{\textit{The Rust Book}} by Steve Klabnik and Carol Nichols
    \item \href{https://www.rust-lang.org/learn}{\textit{Learn Rust}}, the official references and tutorials for Rust
    \item \href{https://doc.rust-lang.org/reference/index.html}{\textit{The Rust Reference}}
    \item \href{https://doc.rust-lang.org/nomicon/index.html}{\textit{The Rustonomicon},} the reference book for Unsafe Rust
    \item \href{https://doc.rust-lang.org/nightly/unstable-book/index.html}{\textit{The Unstable Book},} the reference book for Rust's nightly features
    \item \href{https://snake.rustbridge.com/}{\textit{The Rusty Snake Book},} a tutorial on Rust by Rustbridge
    \item \href{https://rust-quest.com}{\textit{Rust Quest},} a tutorial on Rust by Liam Garriga
    \item \href{https://rustacean.net}{\textit{Website for Ferris the Crab}}
    \item \href{https://tfpk.github.io/lifetimekata/index.html}{\textit{LifetimeKata},} a tutorial on lifetimes by Tom Kunc
    \item \href{https://huonw.github.io/blog/2016/04/myths-and-legends-about-integer-overflow-in-rust}{\textit{Myths and Legends about Integer Overflow in Rust},} a blog post by Huon Wilson
\end{enumerate}

\end{document}
