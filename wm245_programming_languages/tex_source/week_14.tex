\documentclass[11pt,oneside,a4paper]{article}
\usepackage[%
	a4paper,%
	left = 20mm,%
	right = 20mm,%
	textwidth = 178mm,%
	top = 35mm,%
	bottom = 30mm,%
	headheight=70pt,%
headsep=25pt,%
]{geometry}
\usepackage{graphicx}
\usepackage[inkscapeformat=png]{svg}
\usepackage{transparent}
\usepackage{lastpage}

\usepackage{fontspec}
\setmainfont{Montserrat}
\setmainfont{Noto Sans}

\input{helper/preamble.tex}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Edit below
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\def\weekno{14}
\def\labtitle{Introduction to Haskell}
\input{module_info}

\setmainfont[
    UprightFont = {Montserrat Light},
    BoldFont = {Montserrat Bold},
    ItalicFont = {Montserrat Italic},
    BoldItalicFont = {Montserrat Bold Italic}
]{Montserrat}

\input{helper/header.tex}

\title{Week \weekno{}: \labtitle{}}
\author{\moduleno{} \moduletitle{}}
\date{January 2024}

\begin{document}

\begin{center}
    \LARGE{\textbf{\moduleno{}: \moduletitle{}}}

    \Large{Week \weekno{}: \labtitle{}}

    \vspace{0.5cm}
    
    \includegraphics[height=4cm]{img/haskell_logo.png}
    
    \vspace{0.5cm}
\end{center}

The next two labs will introduce you to functional programming using Haskell. Haskell markets itself as ``\emph{an advanced, purely functional programming language}'', and, in learning it, you will learn to embrace using functional programming to solve problems, and get an appreciation for its benefits.

\section{What is functional programming?}

In previous modules, you learned how to program using \emph{procedural} languages. In a procedural language, a program acts like a step-by-step guide, telling the computer how to behave with each line of code. An important part of any procedural program is its state, the variables stored by the program. Programs can modify their state, and alter their behaviour depending on their state.

As an illustration, consider the following procedurally written Fizzbuzz program:

\begin{minted}[frame=single,framesep=4pt, fontfamily=tt, escapeinside=||, mathescape=true, linenos]{python}
i = 1
while i < 100:
    if (i % 15) == 0:
        print("Fizzbuzz")
    else if (i % 3) == 0:
        print("Fizz")
    else if (i % 5) == 0:
        print("Buzz")
    else:
        print(str(i))
    i += 1
\end{minted}

This program stores a variable \texttt{i}. Throughout its execution, the value of \texttt{i} changes, and the program's execution on each iteration changes depending on the value of \texttt{i}.

In contrast, functional programming aims to be ``pure'' or stateless. Functional programs do not use variables (but they may use constants), and functions do not have side effects. In many ways, this makes functional programming the antithesis of languages like C and Assembly, where side effects are the \emph{primary} way of making things happen.

So if functional programs don't have state, how do they make progress? By making each function call another function! By repeatedly calling functions, and encoding a program's ``state'' in function arguments, we can cleverly get functional programs to behave procedurally despite only ever executing a single function at a time. For this reason, functional languages often favour recursive programming.

A more functional version of the Fizzbuzz program is shown below\footnote{This program is arguably still slightly impure, because \texttt{fizzbuzz} procedurally prints a string before making the next recursive call. In reality, most functional programming languages are slightly impure, because they need to be able to perform I/O operations.}:

\begin{minted}[frame=single,framesep=4pt, fontfamily=tt, escapeinside=||, mathescape=true, linenos]{python}
def fizzbuzz(n: default=1, max: default=100):
    if i >= 100: return end
    
    if (i % 15) == 0:
        print("Fizzbuzz")
    else if (i % 3) == 0:
        print("Fizz")
    else if (i % 5) == 0:
        print("Buzz")
    else:
        print(str(i))

    return fizzbuzz(n+1, max)
\end{minted}

To further encourage the use of pure functions, functional languages typically treat functions as first-class objects. This allows functions to be passed as arguments, returned by other functions, and composed with each other -- enabling functions to be created and moved around without having to resort to side effects.

To demonstrate how first-class functions work, consider the \texttt{map} function, which is common in functional languages:

\begin{minted}[frame=single,framesep=4pt, fontfamily=tt, escapeinside=||, mathescape=true, linenos]{python}
>>> map(square, [1, 2, 3])
[1, 4, 9]
>>> map(
        lambda x: x + 1, 
        [1, 2, 3]
    )
[2, 3, 4]
\end{minted}

% First-class functions get much more powerful than this, and we will see this later on.

\section{Why functional programming?}

The typical advantages that functional languages offer over procedural languages include: \begin{itemize}
    \item The source code tends to be easier to read
    \item It is easier to prove correctness in (and formally verify) software
    \item It is harder to accidentally introduce runtime errors into software
    \item Functional languages are usually incredibly type-safe (and don't crash at runtime)
    \item Functional languages tend to feel ``nicer'' to use
\end{itemize}

Don't underestimate the last point -- using a programming language you enjoy using makes programming much more enjoyable, and it can make a huge impact on your job satisfaction as a software or security engineer.

That said, functional languages offer trade-offs just like procedural languages, and each language has its own advantages and disadvantages. In fact, many procedural languages now include features from functional languages, and vice-versa.

Finally, some tasks naturally lend themselves well to functional programming. For example, building a compiler (which is notoriously hard in languages like C) suddenly becomes easy in a language like Haskell! Other programs which are often written in functional languages include financial analysis software, proof assistants, hardware design tools and simulation software.

\exercise{
    \begin{enumerate}
        \item \href{https://github.com/trending/haskell}{Check out GitHub's ``Trending'' page to see examples of software written in Haskell}.
        \item (Optional) There are other benefits to functional. \href{https://typeable.io/blog/2021-02-26-fp-pros.html}{Check out this Typeable article for a deeper look into the advantages of functional programming}.
    \end{enumerate}
}

% If you are new to functional programming, you will likely have to accept these points as fact for the time being, but you will begin to see them during the labs as you get experience with using Haskell.
%
%\section{How to use this lab}
%
%Because functional programming is likely to be new to many of you, this lab sheet provides plenty of exercises to get you used to functional programming. Do a few exercises from each section -- you do not need to do all of the tasks (unless you want to).
%
%This lab sheet will focus on getting you used to Haskell. Next week's lab sheet will focus on using Haskell to write fully-fledged software. Don't worry about becoming fluent in Haskell by then (as we'll provide you with template code), but you should at least be able to understand how Haskell works.
\newpage

\section{Installing Haskell}

While the labs for this week can be completed using a web-based environment, you may find it useful to install Haskell for next week's labs. Installing Haskell involves installing two components: a compiler (normally \href{https://www.haskell.org/ghc}{GHC}) and a build tool (normally either \href{https://www.haskell.org/cabal}{Cabal} or \href{https://docs.haskellstack.org/en/stable}{Stack}).

The easiest way to install Haskell is using \href{https://www.haskell.org/ghcup}{GHCup}.

\exercise{\begin{enumerate}
    \item \href{https://www.haskell.org/ghcup/install}{Install Haskell using the instructions here}.
\end{enumerate}}

(This is a good opportunity to remind you that running \texttt{sudo sh} with unknown bash scripts is dangerous. While many open-source programs will ask you to install them with \texttt{curl ... | sudo sh}, remember that this command downloads and runs an unknown script from the internet with root privileges. \textbf{Do not run unknown scripts as root unless you have verified and completely trust the code.}
\headerspace{}

\subsection{Online environments}

There are numerous web-based programming environments for Haskell. Some of the more popular ones are shown below.

\begin{itemize}
    \item \href{https://replit.com/languages/haskell}{Replit}
    \item \href{https://play.haskell.org}{Haskell Playground}
    \item \href{https://www.tutorialspoint.com/compile_haskell_online.php}{Tutorialspoint}
\end{itemize}

They are good for ``quick and dirty'' code, but fall short for fully-fledged programs due to their limited I/O support.
\headerspace{}

\subsection{Jupyter notebooks}

At present, notebooks such as Jupyter and Google Colab don't support Haskell.
\newpage

\section{Our first program}

The ``Hello, world'' program in Haskell looks like the following:

\begin{minted}[frame=single,framesep=4pt, fontfamily=tt, escapeinside=||, mathescape=true, linenos]{haskell}
main :: IO ()
main = putStrLn "Hello, world!"
\end{minted}

Line 1 in the snippet above declares that \texttt{main} has type \texttt{IO ()}. This is a special datatype that allows the computer to interact with I/O. We do not need to include line 1 as the type checker will infer its type, but it is considered good practice to explicitly annotate its type.

For now, don't worry about what goes on in \texttt{main}. We will only use it to print the results of the functions we write.
\headerspace{}

\subsection{Compiling our program}

Before we compile our program, we must save it to a file. The extension for Haskell files is \texttt{.hs}. After that, we can compile our program using the \texttt{ghc} command.

\begin{minted}[frame=single,framesep=4pt, fontfamily=tt, escapeinside=||, mathescape=true, linenos]{bash}
$ ghc -o hello hello.hs
\end{minted}

We can then run our program just like any other binary.

\begin{minted}[frame=single,framesep=4pt, fontfamily=tt, escapeinside=||, mathescape=true, linenos]{bash}
$ ./hello
Hello, world!
\end{minted}

\section{Getting used to Haskell}

\subsection{Variables}

We assign values to variables using the \emph{equals} operator \texttt{=}.

\begin{minted}[frame=single,framesep=4pt, fontfamily=tt, escapeinside=||, mathescape=true, linenos]{haskell}
x = 4
\end{minted}

In Haskell, however, variables are not actually variables; they are constants, and, once we have declared a variable \texttt{x}, we can't change its value anymore. Think of \texttt{x = 4} as saying ``\texttt{x} \emph{is defined to be} \texttt{4}'' instead of ``\emph{Assign} \texttt{4} \emph{to} \texttt{x}.''

We can also declare the type of \texttt{x = 4} before giving it a value, which is useful for avoiding unexpected type errors later down the line.

\begin{minted}[frame=single,framesep=4pt, fontfamily=tt, escapeinside=||, mathescape=true, linenos]{haskell}
x :: Int
x = 4
\end{minted}
\newpage

Because values are not variable, Haskell will complain if you attempt to define the same variable multiple times.

\begin{minted}[frame=single,framesep=4pt, fontfamily=tt, escapeinside=||, mathescape=true, linenos]{haskell}
x :: Int
x = 4
x = 5       -- Bad
\end{minted}
\headerspace{}

\subsection{Comments}

Haskell supports both single-line and multiline comments:

\begin{minted}[frame=single,framesep=4pt, fontfamily=tt, escapeinside=||, mathescape=true, linenos]{haskell}
-- This is a single-line comment
{- This is a
              multiline comment -}
\end{minted}
\headerspace{}

\subsection{Basic types}

The standard datatype for integers in Haskell is \texttt{Int}, a signed integer type. Its range is implementation-dependent, but is guaranteed to be at least $\pm 2^{29}$.

\begin{minted}[frame=single,framesep=4pt, fontfamily=tt, escapeinside=||, mathescape=true, linenos]{haskell}
ex1 : Int
ex1 = -1
\end{minted}

For larger integers, Haskell also supports the \texttt{Integer} datatype. The range of \texttt{Integer} is unbounded, and is limited only by the amount of memory on your machine.

\begin{minted}[frame=single,framesep=4pt, fontfamily=tt, escapeinside=||, mathescape=true, linenos]{haskell}
ex2 :: Integer
ex2 = 1234567890987654321987340982334987349872349874534

ex3 :: Integer
ex3 = 2^(2^(2^(2^2)))

ex3Length :: Int
ex3Length = length (show reallyBig)
\end{minted}

For floating-point numbers, Haskell supports the \texttt{Float} and \texttt{Double} datatypes.

\begin{minted}[frame=single,framesep=4pt, fontfamily=tt, escapeinside=||, mathescape=true, linenos]{haskell}
ex4 :: Float
ex4 = 3.1415

ex5 :: Double
ex5 = 4.51e-7
\end{minted}
\newpage

For Boolean variables, we have the \texttt{Boolean} datatype.

\begin{minted}[frame=single,framesep=4pt, fontfamily=tt, escapeinside=||, mathescape=true, linenos]{haskell}
ex6, ex7 :: Bool
ex6 = True
ex7 = False
\end{minted}

Finally, for characters and strings, Haskell has \texttt{Char} and \texttt{String}. Characters in Haskell use the Unicode encoding, and strings are essentially lists of characters for all intents and purposes.

\begin{minted}[frame=single,framesep=4pt, fontfamily=tt, escapeinside=||, mathescape=true, linenos]{haskell}
ex8, ex9 :: Char
ex8 = 'x'
ex9 = '天'

ex10 :: String
ex10 = "Hello, Haskell!"
\end{minted}

Notice that we must use single quotes for \texttt{Char}s and double quotes for \texttt{String}s.
\headerspace{}

\subsection{Arithmetic}

Haskell supports all the expected arithmetic expressions.

\begin{minted}[frame=single,framesep=4pt, fontfamily=tt, escapeinside=||, mathescape=true, linenos]{haskell}
ex11 = 3 + 2
ex12 = 19 - 27
ex13 = 2.35 * 8.6
ex14 = 8.7 / 3.1
ex15 = mod 19 3
ex16 = 19 `mod` 3
ex17 = 7 ^ 222
ex18 = (-3) * (-7)

ex19  = 3 + 0.5            -- Bad: can only operate on two values of the same type
ex20 = 3 + (round 0.5)          -- Ok
ex21 = 0.5 + (fromIntegral 0.5) -- Ok

ex22 = 10 / 10                  -- Bad: / can't be used to perform integer division
ex23 = 10 `div` 10              -- Ok (returns an Int)
ex24 = 12 `div` 5               -- Ok (returns an Int)
\end{minted}

Notice that we can use \texttt{`}backticks\texttt{`} to turn \texttt{div} and \texttt{mod} into infix operators. If we don't do this, they instead behave as regular functions, and we must place their arguments \emph{after} the operators.

\exercise{\begin{enumerate}
    \item How can we cast an \texttt{Int} to a \texttt{Double} in Haskell? How about vice versa?
    \item Write a program that calculates \texttt{1.5} + \texttt{1.5}. What's interesting about this program?
    \item Write a program that calculates \texttt{1} + \texttt{0.5}. What problems do you encounter?
    \item How can we round an integer to the nearest integer in Haskell?
    \item Write a program that rounds an integer to the next multiple of 5. What problems do you encounter?
\end{enumerate}}
\headerspace{}

\subsection{Boolean logic}

We've seen already that Haskell contains the Boolean expressions \texttt{True} and \texttt{False}. Haskell also allows Boolean values to be combined to form more complex expressions.

\begin{minted}[frame=single,framesep=4pt, fontfamily=tt, escapeinside=&&&&, mathescape=true, linenos]{haskell}
ex25, ex26, ex27 :: Bool
ex25 = True && True               -- Returns True
ex26 = False || True              -- Returns True
ex27 = not (False && True)        -- Returns True
\end{minted}

Haskell supports \texttt{if} expressions. However, unlike other languages, all \texttt{if} expressions must have an \texttt{else} clause (because they must always have the same type).

\begin{minted}[frame=single,framesep=4pt, fontfamily=tt, escapeinside=||, mathescape=true, linenos]{haskell}
ex1 :: Int
ex1 = if x + 3 == 7
    then 1
    else 2
\end{minted}

As you might expect, \texttt{if b then e\_true else e\_false} gives \texttt{e\_true} if \texttt{b} evaluates to \texttt{True},and \texttt{False} otherwise. It's also useful to know that, if \texttt{b} is \texttt{True}, \texttt{e\_false} is never evaluated, and vice-versa whenever \texttt{b} is True. This makes Haskell a lazy language, and helps improve runtime efficiency.

\begin{minted}[frame=single,framesep=4pt, fontfamily=tt, escapeinside=&&&&, mathescape=true, linenos]{haskell}
ex28 :: Bool -> Bool
ex28 x = if x == True
    then True
    else ex28 x
\end{minted}

Amazingly, the above program will still compile, and will only break when you call \texttt{ex28 False}.

However, \texttt{if} expressions are much rarer in Haskell than they are in other languages, because Haskell also supports \emph{pattern matching} (which we will see later).
\newpage

\subsection{Lists}

In Haskell, lists are a recursively defined data structure.

The smallest possible list is the empty list:

\begin{minted}[frame=single,framesep=4pt, fontfamily=tt, escapeinside=||, mathescape=true, linenos]{haskell}
emptyList1 = []          -- Empty list of unspecified type

emptyList2 :: [Integer]
emptyList2 = []          -- Empty list of type Integer
\end{minted}

All other lists are built up from the empty list using the \emph{cons} operator (\texttt{:}). If \texttt{L} is a list, then \texttt{x : L} produces a new list with \texttt{x} at the front of \texttt{L}. In this way, lists behave much like linked lists.

\begin{minted}[frame=single,framesep=4pt, fontfamily=tt, escapeinside=||, mathescape=true, linenos]{haskell}
ex29 =           4 : []
ex30 =      3 :  4 : []
ex31 = 2 : (3 : (4 : []))

ex32 = 2 : 3 : 4 : [] == [2, 3, 4]    -- Returns True
\end{minted}

We can see from \texttt{ex4} that \texttt{[2, 3, 4]} is syntactic sugar for \texttt{2 : 3 : 4 : []}.
\headerspace{}

\subsection{Special syntax for lists}

Haskell supports a few more special kinds of syntax.

\begin{minted}[frame=single,framesep=4pt, fontfamily=tt, escapeinside=||, mathescape=true, linenos]{haskell}
ex33 = [1, 2, 3, 19]
ex34 = [1..100]
ex35 = [2..100]
ex36 = [2, 4..100]
\end{minted}

If this is confusing to you, don't worry! These aren't required when programming in Haskell.
\headerspace{}

\subsection{Strings}

Strings in Haskell are considered to be lists of \texttt{Char}s.

\begin{minted}[frame=single,framesep=4pt, fontfamily=tt, escapeinside=||, mathescape=true, linenos]{haskell}
ex37 :: [Char]
ex37 = ['H', 'e', 'l', 'l', 'o']

ex38 :: String
ex38 = "Hello"

helloSame = ex37 == ex38
\end{minted}

This also means that any function which works on lists also works on strings.

\section{Functions}

This is where Haskell becomes interesting (and unusual!). In Haskell, functions have the type \texttt{a -> b}, where \texttt{a} is the type of the argument and \texttt{b} is the return type.

\begin{minted}[frame=single,framesep=4pt, fontfamily=tt, escapeinside=||, mathescape=true, linenos]{haskell}
f :: Int -> Int
f x = x + 3

f 3 == 6           -- Returns True
\end{minted}

Notice that we don't use brackets for the function arguments -- instead, we separate each argument with a space.

What happens when a function has more than one argument? Observe:

\begin{minted}[frame=single,framesep=4pt, fontfamily=tt, escapeinside=||, mathescape=true, linenos]{haskell}
f :: Int -> Int -> Int
f x y = x + y

f 3 4 == 7          -- Returns True
\end{minted}

But wait, why does \texttt{f} have type \texttt{Int -> Int -> Int}? Doesn't that imply that, when we provide an integer to \texttt{f}, it returns another function? Yes, it does! Functions are considered first-class citizens in Haskell, and this might well be the most confusing aspect of functional programming.

\begin{minted}[frame=single,framesep=4pt, fontfamily=tt, escapeinside=||, mathescape=true, linenos]{haskell}
-- Add two numbers together
f1 :: Int -> Int -> Int
f1 x y = x + y

-- Add 3 to a number
f2 :: Int -> Int
f2 = f1 3

f2 4 == 7          -- Returns True
\end{minted}

\exercise{\begin{enumerate}
    \item Write a function \texttt{multiple x y} which returns \texttt{True} if \texttt{y} is a multiple of \texttt{x}, and \texttt{False} otherwise.
    \item Use your implementation of \texttt{multiple} to write a function \texttt{isEven x}, which returns \texttt{True} if \texttt{x} is even, and \texttt{False} otherwise.
    \item By using a partial application of \texttt{div}, write a function \texttt{halve x} which halves the value of \texttt{x}, returning an \texttt{Integer}.
\end{enumerate}}
\newpage

\subsection{Recursive functions}

Many of the more useful functions in Haskell are recursive. In fact, Haskell supports this by providing pattern-matching, which allows programs to evaluate functions by case analysis on the argument.

\begin{minted}[frame=single,framesep=4pt, fontfamily=tt, escapeinside=||, mathescape=true, linenos]{haskell}
-- Compute the sum of the integers from 1 to n
nSumtorial, sumtorial :: Int -> Int
nSumtorial n = if n == 0
    then 0
    else n + nSumtorial (n-1)

sumtorial 0 = 0
sumtorial n = n + sumtorial (n-1)
\end{minted}

We can operate on lists in the same way:

\begin{minted}[frame=single,framesep=4pt, fontfamily=tt, escapeinside=||, mathescape=true, linenos]{haskell}
-- Calculate the sum of numbers in a list
sum :: Num a => [a] -> a
sum [] = 0
sum (x : xs) = x + sum xs

-- Apply a function f to all elements in a list
map :: (a -> a) -> [a] -> a
map f [] = []
map f (x : xs) = (f x) : (map f xs)

-- Calculate n mod every item in a list
modList :: Int -> [Int] -> Int
modList n x = map (mod n) x            -- Notice the order of arguments here
\end{minted}

\exercise{\begin{enumerate}
    \item Write a function \texttt{addOneList list} which adds \texttt{1} to every element in a list of integers.
    \item Write a function \texttt{combineLists list1 list2} which adds the elements of \texttt{list2} to the end of \texttt{list1}.
    \item Write a function \texttt{interleaveLists list1 list2} which combines the elements of \texttt{list1} and \texttt{list2} by interleaving them as far as possible. For the longer list, the elements should be added to the end of the list.
    \item Write a function \texttt{reverse list} which reverses the elements of \texttt{list}.
    \item Try the exercise \emph{Validating Credit Card Numbers} from \href{https://www.cis.upenn.edu/~cis1940/spring13/hw/01-intro.pdf}{UPenn's Haskell exercises}.
\end{enumerate}}
\newpage

\section{Optimising recursion}

Consider the function \texttt{sumtorial} defined below:

\begin{minted}[frame=single,framesep=4pt, fontfamily=tt, escapeinside=||, mathescape=true, linenos]{haskell}
nsum :: Int -> Int
sumtorial 0 = 0
sumtorial n = n + sumtorial (n-1)
\end{minted}

What would the function calls look like on a stack normally?

\begin{minted}[frame=single,framesep=4pt, fontfamily=tt, escapeinside=||, mathescape=true, linenos]{text}
sumtorial 5
= 5 + (sumtorial 4)
= 5 + (4 + (sumtorial 3))
= 5 + (4 + (3 + (sumtorial 2)))
= 5 + (4 + (3 + (2 + (sumtorial 1))))
= 5 + (4 + (3 + (2 + (1 + (sumtorial 0)))))
= 5 + (4 + (3 + (2 + (1 + (0)))))
= 5 + (4 + (3 + (2 + (1)))
= 5 + (4 + (3 + (3)))
= 5 + (4 + (6))
= 5 + (10)
= 15
\end{minted}

That's a lot of function calls! In many programming languages, this would make recursive functions unviable due to the amount of stack space needed for each function call. Fortunately, Haskell has a neat optimisation for that: each function call and reduction is applied straight away, meaning we always use the same stack frame! In fact, the only extra space we need is to store each of the numbers we're adding, and those numbers can easily be stored on the same stack frame.

In languages without such optimisations, we would need to add a \emph{tail-recursive} optimisation to our function so that the function \emph{only} calls another instance of itself.

\begin{minted}[frame=single,framesep=4pt, fontfamily=tt, escapeinside=||, mathescape=true, linenos]{haskell}
nsum :: Int -> Int
sumtorial n = sum n 0 where
    sum 0 total = total
    sum n total = sum (n - 1) (total + n)
\end{minted}

Notice how \texttt{sum} encodes the total in its argument so that it doesn't need to any extra variables or expressions with each call. This way, each time \texttt{sum} makes a recursive call, its stack space can be repurposed for the new \texttt{sum} call\footnote{Our function also made use of \texttt{where} to store an inner function, and it's well worth learning about how \texttt{let} and \texttt{where} work, but these are optional features.}.

\href{https://stackoverflow.com/questions/13042353/does-haskell-have-tail-recursive-optimization}{Stack Overflow has a great question explaining the optimisations made.}

\end{document}
