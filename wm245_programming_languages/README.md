# WM245 - Programming Languages for Cyber Security

## Syllabus

* **Week 1:** Introduction to Software Vulnerabilities
* **Week 2:** Getting Started with Java
* **Week 3:** (reworking)
* **Week 4:** (reworking)
* **Week 5:** Concurrency and Multithreading
* **Week 6:** (reading week)
* **Week 7:** Event-Driven Programming
* **Week 8:** Compilers
* **Week 9:** (reworking)
* **Week 10:** Reflecting on Reflection
* **Week 11:** Event-Driven Programming and Multithreading in Java
* **Week 12:** Documentation and Code Extensibility
* **Week 13:** Networking in Programming
* **Week 14:** Introduction to Haskell
* **Week 15:** Using Haskell to write Software
* **Week 16:** (reading week)
* **Week 17:** Introduction to Prolog
* **Week 18:** Prolog in the Wild
* **Week 19:** Artificial Intelligence in Software Engineering
* **Week 20:** Vulnerability Hunting in Open-Source Software
