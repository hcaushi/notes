\documentclass[11pt,oneside,a4paper]{article}
\usepackage[%
	a4paper,%
	left = 20mm,%
	right = 20mm,%
	textwidth = 178mm,%
	top = 35mm,%
	bottom = 30mm,%
	headheight=70pt,%
headsep=25pt,%
]{geometry}
\usepackage{graphicx}
\usepackage[inkscapeformat=png]{svg}
\usepackage{transparent}
\usepackage{lastpage}
\usepackage{exsheets}
\usepackage{fontspec}
\setmainfont{Montserrat}
\setmainfont{Noto Sans}

\input{helper/preamble.tex}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Edit below
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\def\weekno{13}
\def\labtitle{Java Networking}
\input{module_info}

\setmainfont[
    UprightFont = {Montserrat Light},
    BoldFont = {Montserrat Bold},
    ItalicFont = {Montserrat Italic},
    BoldItalicFont = {Montserrat Bold Italic}
]{Montserrat}

\input{helper/header.tex}

\title{Week \weekno{}: \labtitle{}}
\author{\moduleno{} \moduletitle{}}
\date{January 2024}

\begin{document}

\begin{center}
    \LARGE{\textbf{\moduleno{}: \moduletitle{}}}

    \Large{Week \weekno{}: \labtitle{}}

    \vspace{0.5cm}
    
    \includegraphics[height=4cm]{img/Java-Emblem.jpg}
    
    \vspace{0.5cm}
\end{center}

Networking in Java involves using classes and APIs to enable communication between different devices over a network. It has applications in various scenarios, including client-server architectures, web development, and distributed computing. Developing skills in Java networking is vital for developers building scalable and interconnected systems, allowing them to create applications that can communicate and share information across networks.

\section{Introduction to Networking in Java}

Java provides a comprehensive set of classes and APIs within the java.net package to support networking tasks.

Some important classes include:
\begin{itemize}
    \item \texttt{Socket}: Represents a client-side socket that can connect to a server.
    \item \texttt{ServerSocket}: Represents a server-side socket that listens for incoming client connections.
    \item \texttt{URL}: Represents a URL and can be used to open a connection to a resource identified by a URL.
    \item \texttt{URLConnection}: Represents a communication link between the application and a URL.
\end{itemize}

One fundamental concept in Java networking is the use of sockets. A socket is a communication endpoint that enables bidirectional data flow between a client and a server. The \texttt{Socket} class is employed for client-side communication, allowing Java applications to connect to servers, while the \texttt{ServerSocket} class is used on the server side to listen for incoming client connections. This client-server model forms the basis for many networked applications where a server waits for client requests, and clients establish connections to communicate with the server.

Java's networking capabilities extend beyond basic socket communication. The \texttt{URL} class allows applications to open connections to remote resources, retrieve data, and interact with web services. The \texttt{URLConnection} class complements \texttt{URL} by providing a means to connect to and exchange data with the specified URL.

\section{Java Sockets}
Sockets allow communication using input and output streams. The \texttt{InputStream} and \texttt{OutputStream} classes allow the exchange of data between connected entities. Clients use a \texttt{Socket} to initiate a connection to a server, while servers use a \texttt{ServerSocket} to listen for incoming client connections.

\subsection{Client-Side Socket}
Consider a simple Java program where a client connects to a server using a socket. The client sends a message to the server, and the server responds. The following is a basic example:
\begin{minted}[frame=single,framesep=4pt, fontsize=\scriptsize, fontfamily=tt, escapeinside=||, mathescape=true, linenos]{java}
import java.io.*;
import java.net.*;

public class SimpleSocketClient {
    public static void main(String[] args) {
        try (Socket socket = new Socket("localhost", 5000);
             PrintWriter out = new PrintWriter(socket.getOutputStream(), true);
             BufferedReader in = new BufferedReader(new InputStreamReader(socket.getInputStream()))) {

            // Sending a message to the server
            out.println("Hello, Server!");

            // Receiving and printing the server's response
            String response = in.readLine();
            System.out.println("Server response: " + response);

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
\end{minted}

\subsection{Server-Side Socket}
On the server side, the following program listens for incoming client connections, reads the client's message, and sends a response:
\begin{minted}[frame=single,framesep=4pt, fontsize=\scriptsize, fontfamily=tt, escapeinside=||, mathescape=true, linenos]{java}
import java.io.*;
import java.net.*;

public class SimpleSocketServer {
    public static void main(String[] args) {
        try (ServerSocket serverSocket = new ServerSocket(5000)) {
            System.out.println("Server waiting for clients...");

            while (true) {
                Socket clientSocket = serverSocket.accept();
                System.out.println("Client connected: " + clientSocket.getInetAddress());

                // Handling client communication in a separate thread
                new Thread(() -> {
                    try (
                        PrintWriter out = new PrintWriter(clientSocket.getOutputStream(), true);
                        BufferedReader in = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()))
                    ) {
                        // Reading client's message
                        String clientMessage = in.readLine();
                        System.out.println("Received from client: " + clientMessage);

                        // Sending a response back to the client
                        out.println("Hello, Client! I received your message.");

                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }).start();
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
\end{minted}

For successful communication, both the client and server processes must be running at the same time.
To see the interaction between the client and server, follow these steps:
\begin{itemize}
    \item Compile both SimpleSocketClient.java and SimpleSocketServer.java.
    \item Open two terminal windows.
    \item In one window, run the server: java SimpleSocketServer.
    \item In the other window, run the client: java SimpleSocketClient.
    \item Observe the output on both sides to see the communication.
\end{itemize}

\subsection{Cross-Machine Communication}
To run the client and server on different machines, you need to replace "localhost" with the IP address or hostname of the machine where the server is running. Additionally, you should ensure that the network allows communication between the two machines on the specified port.

The code in previous sections has been modified for running the client and server on different machines:

Server Side:
\begin{minted}[frame=single,framesep=4pt, fontsize=\scriptsize, fontfamily=tt, escapeinside=||, mathescape=true, linenos]{java}
import java.io.*;
import java.net.*;

public class SimpleSocketServer {
    public static void main(String[] args) {
        try (ServerSocket serverSocket = new ServerSocket(5000)) {
            System.out.println("Server waiting for clients...");

            while (true) {
                Socket clientSocket = serverSocket.accept();
                System.out.println("Client connected: " + clientSocket.getInetAddress());

                // Handling client communication in a separate thread
                new Thread(() -> {
                    try (
                            PrintWriter out = new PrintWriter(clientSocket.getOutputStream(), true);
                            BufferedReader in = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()))
                    ) {
                        // Reading client's message
                        String clientMessage = in.readLine();
                        System.out.println("Received from client: " + clientMessage);

                        // Sending a response back to the client
                        out.println("Hello, Client! I received your message.");

                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }).start();
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
\end{minted}

Client Side:
\begin{minted}[frame=single,framesep=4pt, fontsize=\scriptsize, fontfamily=tt, escapeinside=||, mathescape=true, linenos]{java}
import java.io.*;
import java.net.*;

public class SimpleSocketClient {
    public static void main(String[] args) {
        try (Socket socket = new Socket("SERVER_MACHINE_IP", 5000);
             PrintWriter out = new PrintWriter(socket.getOutputStream(), true);
             BufferedReader in = new BufferedReader(new InputStreamReader(socket.getInputStream()))) {

            // Sending a message to the server
            out.println("Hello, Server!");

            // Receiving and printing the server's response
            String response = in.readLine();
            System.out.println("Server response: " + response);

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
\end{minted}

\begin{question}
    Create a simple chat application where multiple clients can connect to a server, send messages, and receive messages broadcasted to all connected clients. Each client should have a unique identifier.
\end{question}

\begin{question}
    Enhance the simple chat application to include private messaging capabilities. Clients can send public messages to the entire chat room or private messages to specific users.
\end{question}

\section{Java URLs}
Working with URLs in Java provides the foundation for building web-related applications, accessing web services, and retrieving data from remote resources. 

\subsection{Creating URL Instances}
The \texttt{URL} class in Java allows you to represent and manipulate URLs. You can create a URL instance by providing a string representation of the URL:
\begin{minted}[frame=single,framesep=4pt, fontfamily=tt, escapeinside=||, mathescape=true, linenos]{java}
import java.net.*;

public class URLExample {
    public static void main(String[] args) {
        try {
            // Creating a URL instance
            URL url = new URL("https://www.example.com");
            System.out.println("URL: " + url);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
    }
}
\end{minted}

\subsection{Opening Connections}
Once you have a URL instance, you can open a connection to the specified resource using \texttt{openConnection()} method. This returns a \texttt{URLConnection object}:
\begin{minted}[frame=single,framesep=4pt, fontsize=\scriptsize, fontfamily=tt, escapeinside=||, mathescape=true, linenos]{java}
import java.io.*;
import java.net.*;

public class URLConnectionExample {
    public static void main(String[] args) {
        try {
            URL url = new URL("https://www.example.com");
            URLConnection connection = url.openConnection();

            // Reading data from the connection
            try (BufferedReader in = new BufferedReader(new InputStreamReader(connection.getInputStream()))) {
                String inputLine;
                while ((inputLine = in.readLine()) != null) {
                    System.out.println(inputLine);
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
\end{minted}

\begin{question}
    Create a Java program that takes a URL as input, opens a connection to that URL, and displays the content of the web page.
\end{question}

\begin{question}
    Write a program that takes the URL of a file as input, opens a connection, and downloads the file to the local machine. Ensure proper error handling for non-existing URLs or connectivity issues.
\end{question}

\end{document}