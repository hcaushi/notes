# Class Notes

<center><img src="https://gitlab.com/hcaushi/notes/-/raw/main/img/wmg_logo.png" width="450px"/></center>
<br/><br/>

These are the course notes for the modules I teach to cybersecurity students at the University of Warwick. They are part of the following degree programmes:

* **[MSc Cyber Security Engineering](https://warwick.ac.uk/fac/sci/wmg/study/masters-degrees/cyber-security-engineering)**
* **[MSc Cyber Security Management](https://warwick.ac.uk/fac/sci/wmg/study/masters-degrees/cyber-security-management)**
* **[BSc Cyber Security](https://warwick.ac.uk/fac/sci/wmg/study/undergraduate/cyber-security)**

The course notes include lecture materials, labs and pointers to any virtual machines needed for each module. At times, they also go beyond the syllabus, but I will indicate where this is the case.

## Modules

The modules currently available are:

* **[WM9C5 Management of Cryptosystems](https://courses.warwick.ac.uk/modules/2023/WM9C5-15):** A postgraduate module in applied cryptography
* **[WM3B3 Low-Level Tools and Techniques for Cyber Security](https://courses.warwick.ac.uk/modules/2023/WM3B3-24):** A third-year undergraduate module in malware analysis and exploit development
* **[WM3B2 Algorithms and Complexity](https://courses.warwick.ac.uk/modules/2022/WM3B2-24):** A third-year undergraduate module in algorithms and data structures
* **[WM245 Programming Languages for Cyber Security](https://courses.warwick.ac.uk/modules/2022/WM245-18)**: A second-year undergraduate module introducing various programming features and paradigms and how they affect security
* **[WM242 Implementing Secure Systems](https://courses.warwick.ac.uk/modules/2022/WM242-24):** A second-year undergraduate module in applied cryptography
* **[WM141 Discrete Structures for Cyber Security](https://courses.warwick.ac.uk/modules/2022/WM141-18):** A first-year undergraduate module in discrete mathematics

## Playlist

I sometimes play cute animal videos during sessions to relax the class, particularly during breaks. The playlist can be found [here](https://yewtu.be/playlist?list=PLO17R4HCOqAnh1iQVOv8Qu2zKKe25-6SN).

## Applying to us

Thinking of applying to us for a degree in cybersecurity? Check out [this page](https://warwick.ac.uk/study/undergraduate/courses/bsc-cyber-security) for our undergraduate degree and [these](https://warwick.ac.uk/study/postgraduate/courses/msc-cyber-security-engineering) [pages](https://warwick.ac.uk/study/postgraduate/courses/msc-cyber-security-management) for our postgraduate degrees for more information on how to apply!

## Contact

For any specific questions, you can reach me at `firstname.lastname [at] warwick.ac.uk` or using the PGP public key `BC1C 888D 4B1F 7638 7406 69DC B6B2 4CDC 8E1E B007`.
