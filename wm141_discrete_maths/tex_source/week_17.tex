\documentclass[11pt,oneside,a4paper]{article}
\usepackage[%
	a4paper,%
	left = 20mm,%
	right = 20mm,%
	textwidth = 178mm,%
	top = 35mm,%
	bottom = 30mm,%
	headheight=70pt,%
headsep=25pt,%
]{geometry}
\usepackage{graphicx}
\usepackage[inkscapeformat=png]{svg}
\usepackage{transparent}
\usepackage{lastpage}

\usepackage{tikz}
\usetikzlibrary{shapes,positioning}

\usepackage{fontspec}
\setmainfont{Montserrat}
\setmainfont{Noto Sans}

\input{helper/preamble.tex}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Edit below
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\def\weekno{17}
\def\labtitle{Social Networks}
\input{module_info}

\setmainfont[
    UprightFont = {Montserrat Light},
    BoldFont = {Montserrat Bold},
    ItalicFont = {Montserrat Italic},
    BoldItalicFont = {Montserrat Bold Italic}
]{Montserrat}

\input{helper/header.tex}

\title{Week \weekno{}: \labtitle{}}
\author{\moduleno{} \moduletitle{}}
\date{January 2024}

\begin{document}

\begin{center}
    \LARGE{\textbf{\moduleno{}: \moduletitle{}}}

    \Large{Week \weekno{}: \labtitle{}}

    \vspace{0.5cm}

    \includegraphics[height=4cm]{img/networkx_logo.png}

    \vspace{0.5cm}
\end{center}

This lab aims to analyse social networks from a graph-theoretical perspective, giving an understanding of how social networks form and maintain themselves.

\section{Warm-up}

These warm-up questions aim to help you get started with analysing graphs. For the questions with graphs, don't worry about strong and weak ties -- assume that all ties are the same.

\begin{enumerate}
    \item Suggest how you would divide each of the following graphs into clusters, and identify the bridges between these clusters:
    \begin{enumerate}
        \item \ \newline
        \begin{center}
            \includegraphics[scale=0.53]{img/graph_4.png}
        \end{center} \newpage
        \item \ \newline
        \begin{center}
            \includegraphics[scale=0.4]{img/graph_6.png}
        \end{center}
        \item \ \newline
        \begin{center}
            \includegraphics[scale=0.4]{img/graph_5.png}
        \end{center}
        \item \ \newline
        \begin{center}
            \includegraphics[scale=0.4]{img/graph_3.png}
        \end{center}
        \end{enumerate}
    (\emph{There are multiple valid answers to this question.})
    \item For the graph in 1(d), what are the degrees of the nodes? What do you notice about the way the degrees are distributed?
    \item In addition to the number of friends, we can measure a person's social connectedness using a metric called the \emph{clustering coefficient}. The clustering coefficient of a person is the number of connections between their friends divided by the maximum possible number of ties.
    \newline
    \begin{center}
        \includegraphics[scale=0.35]{img/graph_8.png}
    \end{center}

    For example, in the graph above, Alice has a clustering coefficient of 1/6 (as there is 1 tie between Bob, Charles, Dan and Eden, out of a maximum of 6).
    \begin{enumerate}
        \item In the real world, why do we expect most people to have relatively high clustering coefficients? What would it mean for a person to have a very low clustering coefficient?
        \item Considering only strong friendships, how would you expect strong triadic closure to influence a person's clustering coefficient?
        \item What is the clustering coefficient of Alice in the graph below? What about that of Bob? \newline
        \begin{center}
            \includegraphics[scale=0.53]{img/graph_4.png}
        \end{center}
    \end{enumerate}
    (\emph{Not-so-fun fact: studies have found that teenage girls with low clustering coefficients are significantly more likely to contemplate suicide than those whose clustering coefficients are high.})
    \item A student claims that, in any social network which satisfies the strong triadic closure property, if Alice has at least two strong ties with other people, and is involved in a bridge (or a local bridge), that bridge must be a weak tie. Against all odds, the student is correct.

    Prove the student's claim.
\end{enumerate}

\section{Triadic closure}

\begin{enumerate}
    \item In 2--3 sentences, explain what triadic closure is and how it plays a role in the formation of social networks. You may draw a schematic picture if you find it useful.
    \item For each of the following graphs, state whether the graph satisfies the triadic closure property, briefly justifying your answer:
    \begin{enumerate}
        \item \ \newline
        \begin{center}
            \includegraphics[scale=0.4]{img/graph_5.png}
        \end{center}
        \item \ \newline
        \begin{center}
            \includegraphics[scale=0.4]{img/graph_1.png}
        \end{center}
    \end{enumerate}
    \item Consider the social network below, in which every edge (except for \textit{BC}) is labelled as either strong or weak. The strength of \textit{BC} is unknown.
    \newline
    \begin{center}
        \includegraphics[width=0.7\linewidth]{img/graph_9.png}
    \end{center}

    Given that the graph satisfies the triadic closure property, would you expect \textit{BC} to be a strong or a weak tie? Briefly justify your answer.
    \item In the social graph below, which trios of nodes satisfy the strong triadic closure property, and which do not?
    \begin{center}
        \includegraphics[scale=0.53]{img/graph_4.png}
    \end{center}

    If the group start to form ties through triadic closure, what would the end result be? Draw a possible end graph.
\end{enumerate}

\section{Homophily}

\begin{enumerate}
    \item Give an example of a social network which demonstrates homophily, and an example of a social network which demonstrates heterophily (ie. where most people have ties with people who are \emph{different} to them).
    \item Before analysing graphs for homophily, we may want to convince ourselves that of the \textit{2pq} probability of an edge between two nodes in different groups. We can do this using a simple graph consisting of four blue nodes and four red nodes:

    \begin{center}
        \includegraphics[scale=0.4]{img/graph_11.png}
    \end{center} \newpage

    \begin{enumerate}
        \item How many possible edges are there in the graph?
        \item What are the probabilities \textit{p} and \textit{q} of a given node being a blue node and a red node respectively?
        \item To generate an edge randomly, we select two nodes in the graph uniformly at random. If we generate an edge this way, what is the probability that we generate the edge \textit{AH}?
        \item How many edges connect two blue nodes? Show that the probability of generating an edge between two blue nodes is approximately \textit{p}$^2$.
        \item How many edges connect a blue node and a red node? Show that the probability of generating an edge between a blue node and a red node is approximately 2\textit{pq}.
    \end{enumerate}

    In practice, the values are slightly less than those described above because a person cannot be friends with themselves.

    Another way of thinking about it is in terms of the binomial expansion of (\textit{p} + \textit{q})$^2$. Namely, consider the total probability of choosing any node \textit{p} + \textit{q} = 1. From this, we notice that the probability of choosing any two nodes is (\textit{p} + \textit{q})$^2$, which is also equal to 1.

    (\textit{p} + \textit{q})$^2$ is also equal to \textit{p}$^2$ + 2\textit{pq} + \textit{q}$^2$, where \textit{p}$^2$ is the probability of selecting two red nodes, \textit{q}$^2$ is the probability of selecting two blue nodes, and 2\textit{pq} is the probability of selecting a red node and a blue node. Therefore, the ``2'' is needed to make the total probabilities sum to 1.
    \item In the lecture, we saw the following social graph with male, female and non-binary people: \newline
    \begin{center}
        \includegraphics[scale=0.4]{img/graph_10.png}
    \end{center}

    For the purpose of the question, assume that all weak ties are also strong ties. You are free to decide which colour corresponds to each gender.
    \begin{enumerate}
        \item What are the values of $p_m$, $p_f$ and $p_x$?
        \item If ties are distributed uniformly in the graph, briefly justify why a tie from a male person has probability $p_m$ of connecting them to another male person.
        \item If ties are distributed randomly, briefly justify why a tie from a male person has probability 2$p_f$ and 2$p_x$ of connecting them to a female person and a non-binary person respectively.
        \item Hence explain why a randomly selected edge has probabilities $(p_m)^2$, 2$p_m p_f$ and 2$p_m p_x$ of connecting two male people, a male and a female, and a male and a non-binary person respectively.
        \item What are the actual probabilities you find in the graph? What about for female and non-binary people? Does this show convincing evidence of homophily?
    \end{enumerate}
    \item The graph below shows the social graph of a karate club. A dispute caused the karate club to split into two breakaway clubs, with the node colours used to show the participants' affiliations after the split. \newline
        \begin{center}
            \includegraphics[scale=0.6]{img/graph_7.png}
        \end{center}

    Given that there are 63 friendships in total and that 7 of them are between people of the separate groups,
    \begin{enumerate}
        \item What are the expected edge probabilities $(p_x)^2$, $(p_y)^2$ and 2$p_xp_y$?
        \item What are the observed edge probabilities?
        \item Does the graph show convincing evidence of homophily?
    \end{enumerate}
\end{enumerate}

\section{Betweenness centrality (\emph{Optional})}

We looked at how we might begin identifying clusters of people in the lecture. One way of doing this is by using \emph{betweenness centrality}.

The motivation behind betweenness centrality is that, because bridges and local bridges often connect clusters of people, we should try removing them first. However, we need a way of quantifying this, so that we can identify which bridges to remove first.

To understand how betweenness centrality works, notice that bridges and local bridges often help form shortest paths between nodes in different clusters: without them, the distance between pairs of nodes in these clusters would increase significantly. We therefore define an abstract notion of \emph{traffic} or \emph{flow} on the network and look for the edges that carry the most of this flow. Like bridges and motorways, we might expect these edges to link densely connected regions, and hence to be good candidates for removal.

The Girvan--Newman method uses this principle to identify which edges to remove. It works as follows

\begin{enumerate}
    \item Find the shortest path between each pair of nodes
    \item For each edge in the shortest path, give that edge 1 unit of flow (or, if there are multiple shortest paths, 1/\textit{k} units, where \textit{k} is the number of shortest paths)
    \item Find the edge (or edges) with the highest total flow (or betweenness) and delete all of them from the graph
    \item Repeat steps 1--3 until you are satisfied with the components that remain
\end{enumerate}

For each of the graphs below, perform 2 iterations of the Girvan--Newman method to find the edges that would be removed. They are somewhat finnicky to perform by hand, but you may find the algorithm useful when you implement them programmatically.

\begin{enumerate}
    \item \ \newline
    \begin{center}
        \includegraphics[scale=0.4]{img/graph_2.png}
    \end{center}
    \newpage
    \item \ \newline
    \begin{center}
        \includegraphics[scale=0.35]{img/graph_8.png}
    \end{center}
\end{enumerate}

(\emph{Fun fact: the algorithm perfectly predicted the split that would occur with the karate club in Q3 in the Homophily section, with the exception of Person 9 who was predicted to end up in the white group.})

\section{Using NetworkX}

We can also analyse graphs in Python using the NetworkX library. NetworkX is one of the most popular libraries for graph analysis, and provides interfaces for creating, manipulating and visualising graphs, as well as support for many standard graph algorithms.

Before using NetworkX, you will need to install it using your package manager of choice. This will most likely be PIP (and so your command will be \texttt{pip install networkx}), but if you use Conda or another package manager, you will need to use that to install it. Afterwards, you can import it into your code using its package name \texttt{networkx}.

NetworkX provides a full tutorial \href{https://networkx.org/documentation/stable/tutorial.html}{here}, and it is recommended that you use it, but we will go through the basic usage below.

We start by creating a \texttt{Graph} object. This object will represent our graph, and will store the associated nodes and edges.

\begin{minted}[frame=single,framesep=4pt, fontfamily=tt, escapeinside=||, mathescape=true, linenos]{python}
import networkx as nx

G = nx.Graph()
\end{minted}
\newpage

Once we have a graph, we can add nodes to it. To add a node, we need to specify an ID (which can be any hashable object, such as an integer, string or even an image). We can add nodes either individually, or as a batch using a list or other data structure, and can also add associated data to each node.

\begin{minted}[frame=single,framesep=4pt, fontfamily=tt, escapeinside=||, mathescape=true, linenos]{python}
import networkx as nx

...

G.add_node(1)
G.add_nodes_from([1, 2, 3])
G.add_nodes_from([
    (4, {"color": "red"}),
    (5, {"color": "green"}),
])
G.remove_node(2)
G.remove_nodes_from([1, 3, 4, 5])
\end{minted}

After that, we can add edges between nodes. For each edge, we must specify a source and a target node, and can also choose to add other labels or data.

\begin{minted}[frame=single,framesep=4pt, fontfamily=tt, escapeinside=||, mathescape=true, linenos]{python}
import networkx as nx

...

G.add_edge(1, 2)
G.add_edges_from([(1, 2), (1, 3)])
G.add_edge(1, 4, strength="weak")
G.remove_edge(1, 3)
G.remove_edges_from([(1, 2), (1, 3)])
\end{minted}

Once we have a graph ready, we are ready to visualise it! NetworkX draws graphs using Matplotlib's Pyplot (which must also be installed using PIP).

\begin{minted}[frame=single,framesep=4pt, fontfamily=tt, escapeinside=||, mathescape=true, linenos]{python}
import networkx as nx
import matplotlib.pyplot as plt

...

nx.draw(G)
\end{minted}
\newpage

\subsection{Random graph}

We will start by generating a random graph and analysing its properties.

\begin{enumerate}
    \item Create an undirected graph with 10,000 nodes.
    \item Add 1,000 edges between random \emph{different} edges in your graph. (Note that NetworkX will quietly ignore any nodes or edges which already exist, so take care when adding edges.)
    \item Visualise your graph.
    \item Is your graph connected? If not, what are each of the component sizes?
    \item Find the degrees of each of the nodes.
    \item Now modify your graph so that each node is either red with probabilty 0.4 or blue with probability 0.6. What is the probability of a red node being connected to a red node? How about to a blue node? Does your graph show evidence of homophily?
\end{enumerate}
\vspace{0.4cm}

\subsection{Pokec dataset}

Finally, we will use the \href{https://snap.stanford.edu/data/soc-Pokec.html}{Stanford Pokec dataset} to analyse a real social network. Pokec is a social network that is popular in Slovakia, with around 1.6 million users, and has remained popular even since the launch of Facebook.

\begin{enumerate}
    \item Create an undirected graph using the Pokec dataset. There is a lot of data: the main data that you need to include are the users' genders and friendships.
    \item Is the social graph connected? If not, what are each of the component sizes?
    \item Find the degrees of each of the nodes. Can you identify any bridges or local bridges?
    \item What is the probability of a tie from a male person connecting them to another male person? What about to a female person? Calculate the same probabilities for ties from female people. Do your values show evidence of homophily?
    \item Select a random person in the graph and all of their friends, friends of friends and friends of 3rd degree separation. How many people does your subgraph contain?
    \item Use the Girvan--Newman method to identify the edges with the highest betweenness in the graph, removing edges until you identify the prominent clusters in the subgraph.
    \item Label the edges in your subgraph so that the edges you removed are considered weak ties, and the edges that remain are strong ties. For each person, what proportion of nodes satisfy triadic closure?
    \item Calculate the diameter of the graph, given by the longest shortest path between any two nodes in the graph. Does your diameter support the ``6 degrees of separation'' argument?
\end{enumerate}

\end{document}
