\documentclass[11pt,oneside,a4paper]{article}
\usepackage[%
	a4paper,%
	left = 20mm,%
	right = 20mm,%
	textwidth = 178mm,%
	top = 35mm,%
	bottom = 30mm,%
	headheight=70pt,%
headsep=25pt,%
]{geometry}
\usepackage{graphicx}
\usepackage[inkscapeformat=pdf]{svg}
\usepackage{transparent}
\usepackage{lastpage}

\usepackage{fontspec}
\setmainfont{Montserrat}
\setmainfont{Noto Sans}

\input{helper/preamble.tex}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Edit below
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\def\weekno{15}
\def\labtitle{Monads in Haskell}
\input{module_info}

\setmainfont[
    UprightFont = {Montserrat Light},
    BoldFont = {Montserrat Bold},
    ItalicFont = {Montserrat Italic},
    BoldItalicFont = {Montserrat Bold Italic}
]{Montserrat}

\input{helper/header.tex}

\title{Week \weekno{}: \labtitle{}}
\author{\moduleno{} \moduletitle{}}
\date{January 2024}

\begin{document}

\begin{center}
    \LARGE{\textbf{\moduleno{}: \moduletitle{}}}

    \Large{Week \weekno{}: \labtitle{}}

    \vspace{0.5cm}
    
    \includesvg[height=4cm]{img/haskell_elm.svg}
    
    \vspace{0.5cm}
\end{center}

Last week, we took a first dive into Haskell, and saw how removing side effects creates safe, pure code. But if computations don't have side effects, how do we make anything happen in Haskell? This week, you will learn all about a special feature in Haskell called \emph{monads}, which allow you to write code which does things while remaining safe and easy-to-read.

\section{What are monads?}

Monads are the most complicated part of Haskell, and for good reason! They trace their origins to mathematics, and allow functional languages to include everything from I/O to exception handling to mutable state. In fact, \href{https://wiki.haskell.org/Monad_tutorials_timeline}{there are hundreds of tutorials on monads on the Haskell Wiki}, from theoretical textbooks to ``\textit{I don't care how they work, just show me how to use them!}'' guides. This lab aims to be closer to the latter.

Broadly speaking, a monad is an extension that can be added to a type to say that the type has a side effect when computed. Namely, if \texttt{M} is a monad and \texttt{a} is a type, the monadic type \texttt{M a} contains a value of type \texttt{a}, combined with some computations (characterised by \texttt{M}) that we needed to perform to calculate it.

This is best demonstrated with an example. Consider the \texttt{main} function \texttt{main :: IO ()}. Its type can be split into two parts. Firstly, there's the ``pure'' part of the type, which represents the end result of the function. For \texttt{main}, this is the Unit type \texttt{()}, a unary type whose only value is also \texttt{()} (think of this as the function returning \texttt{Ok})\footnote{This is similar in purpose to \texttt{void} in non-functional languages -- it is only needed because all functions must return \emph{something}.}.

Then there's the ``impure'' or \texttt{monadic} part, which represents the side effects the program will have. For \texttt{main}, we see that the monad is \texttt{IO}, meaning that the function interacts with I/O when called.

So why do we need monadic types? Why declare a value as having type \texttt{M Int} instead of just \texttt{Int}? Well, Haskell is really strict about preventing unexpected runtime errors, and requires you to be \emph{sure} that you know when a function has side effects by requiring you to declare monads in the type definition of functions. Having the type \texttt{M a} ensures that we do not accidentally use a value with side effects in a function which expects a pure or deterministic value.

For example, consider the following function:

\begin{minted}[frame=single,framesep=4pt, fontfamily=tt, escapeinside=||, mathescape=true, linenos]{haskell}
doubleFunctionNumberFromFile :: (FilePath -> Int) -> FilePath -> Int
doubleFunctionNumberFromFile f file = f file + f file

getNumberFromFile :: FilePath -> Int
getNumberFromFile filepath = ...

doubleNumberFromFile :: FilePath -> Int
doubleNumberFromFile = doubleFunctionNumberFromFile getNumberFromFile

main :: IO ()
main = do print (doubleNumberFromFile "~/Documents/secret_number.txt")
\end{minted}

Sounds simple, right? The function \texttt{doubleFunctionNumberInFile} takes a file path, locates an integer in the file and doubles the result.

The problem occurs when \texttt{getNumberFromFile} modifies the number in \texttt{filepath}, which can cause the result to not be double the number originally in the file (or even an even number)\footnote{There is also the issue of the file not containing an integer, but we'll ignore that one in this example.}. To guard against this, Haskell would require us to either ensure that \texttt{getNumberFromFile} cannot modify \texttt{filepath}, or else declare that we are expecting it to have side effects by declaring its type to be \texttt{FilePath -> IO Int}.

\begin{minted}[frame=single,framesep=4pt, fontfamily=tt, escapeinside=||, mathescape=true, linenos]{haskell}
doubleFunctionNumberFromFile :: (FilePath -> IO Int) -> FilePath -> IO Int
doubleFunctionNumberFromFile f file = f file + f file

getNumberFromFile :: FilePath -> IO Int
getNumberFromFile filepath = ...

doubleNumberFromFile :: FilePath -> IO Int
doubleNumberFromFile = doubleFunctionNumberFromFile getNumberFromFile
\end{minted}
\newpage

\section{Common monads}

\subsection{\texttt{Maybe}}

We use the \texttt{Maybe} monad to create the type \texttt{Maybe a}, which is useful when a function might return a value of type \texttt{a}, but it might also be undefined. For any type \texttt{a}, the type \texttt{Maybe a} can take one of two forms:

\begin{itemize}
    \item \texttt{Nothing}
    \item \texttt{Just x}, where \texttt{x :: a}
\end{itemize}

(I personally dislike these names: I much prefer the terms \texttt{None} and \texttt{Some x} as Rust uses.)

Let's try using \texttt{Maybe} to define multiplication and division. Because division by zero is undefined, the \texttt{Nothing} result is appropriate here. hence, \texttt{Maybe} performs a kind of error handling, allowing us to gracefully indicate to other functions that a division has failed.

\begin{minted}[frame=single,framesep=4pt, fontfamily=tt, escapeinside=||, mathescape=true, linenos]{haskell}
divide :: Int -> Int -> Maybe Int
divide x 0 = Nothing
divide x y = Just (x `div` y)

multiply :: Maybe Int -> Maybe Int -> Maybe Int
multiply x Nothing          = Nothing
multiply Nothing y          = Nothing
multiply (Just x) (Just y)  = Just (x * y)
\end{minted}

\exercise{\begin{enumerate}
    \item Haskell provides built-in functions \texttt{isJust} and \texttt{isNothing :: Maybe a -> Bool} for testing whether a \texttt{Maybe} value has the form \texttt{Just x} or \texttt{Nothing}. Write an implementation of these functions. Can you do better than the \href{https://hackage.haskell.org/package/base-4.19.0.0/docs/Data-Maybe.html#v:isJust}{official implementations}?
    \item Write a function \texttt{listJust list} which, given a list \texttt{list} of \texttt{Maybe a}, returns a list of all non-\texttt{Nothing} values in the list. (Hint: you can do this in one line!)
    \item Write a function \texttt{change amt coins} which, given an amount of money \texttt{amt} and a list of coin denominations \texttt{coins}, returns a combination of coins which sum up to \texttt{amt}, or \texttt{Nothing} if \texttt{amt} cannot be made using the coins provided. You can assume that \texttt{coins} is sorted in descending order and that each denomination can be used more than once.
\end{enumerate}}
\newpage

\subsection{\texttt{IO}}

The \texttt{IO} monad indicates that a function performs I/O operations (such as reading from or writing to files) in addition to returning a value. This is useful when a function needs to read from \texttt{stdin} or write to \texttt{stdout} to compute a result.

The most common built-in functions which use \texttt{IO} are as follows:

\begin{minted}[frame=single,framesep=4pt, fontfamily=tt, escapeinside=||, mathescape=true, linenos]{haskell}
-- main can have any IO type, but is normally given type IO ()
main      :: IO a

-- Functions which use stdin/stdout
getChar   :: IO Char
putChar   :: Char -> IO ()
putStrLn  :: String -> IO ()
getLine   :: IO String

-- Functions which read from/write to files
readFile  :: FilePath -> IO String
writeFile :: FilePath -> String -> IO () 
\end{minted}

%https://book.realworldhaskell.org/read/io.html

\exercise{\begin{enumerate}
    \item Write a program which asks for the user's first name, then their last name, then prints their full name. (You might find it helpful to read the \textit{Combining monads} and \texttt{do} sections before attempting this question.)
    \item Write a function \texttt{getPageFromUrl url} which returns the HTML of the page at \texttt{url}. What is its type?
    \item Write a function \texttt{getTextFromFile filepath} that reads a file given by \texttt{filepath} and returns the file's text, or \texttt{Nothing} if the file does not exist. What is its type?
    \item Using your implementation of \texttt{getTextFromFile}, write a function \texttt{getIntFromFile filepath} that returns the first (possibly multi-digit) integer contained in a file, or \texttt{Nothing} if the file does not exist or does not contain an integer.
    \item Write a function \texttt{writeFileSafe filepath string} which attempts to write a string to a file, but fails if the file already exists or the location is not writeable. What is its type?
    \item Write a function \texttt{rollDice n} which rolls an \texttt{n}-sided die and returns the result. First try reading 1 byte from \texttt{/dev/random}, then try reading the \href{https://hackage.haskell.org/package/random-1.2.1.1/docs/System-Random.html}{documentation for \texttt{System.Random}} to write an improved implementation. What is the type of each of your implementations?
\end{enumerate}}
\newpage

\subsection{\texttt{Reader}}

The \texttt{Reader} monad allows functions to read from a global ``variable'' when its value is not known in advance. More importantly, it allows functions to pass its value without marking other functions as monadic. As a motivation, consider the following program:

\begin{minted}[frame=single,framesep=4pt, fontfamily=tt, escapeinside=||, mathescape=true, linenos]{haskell}
data Environment = Environment
  {
    param1 :: Int,
    param2 :: String,
    ...
  }

loadEnv :: IO Environment
loadEnv = ...

func1 :: Environment -> Int
func1 env = param1 env

func2 :: Environment -> String
func2 env = "Result: " ++ (show (func2 env))

main :: IO ()
main = do
  env <- loadEnv
  let str = func1 env
  print str
\end{minted}

Notice that the only function that actually uses the environment is \texttt{func2}, which is a ``pure'' function. It cannot call \texttt{loadEnv}, an ``impure'' function, so the environment has to be passed around as a function argument just so that they can use global data. Wouldn't it be nicer to have a monad just to accommodate these ``pass-through'' variables?

The \texttt{Reader} monad does just that, forming a global data store for read-only data. This means that we no longer need to pass the environment around as an argument for the functions, as the Monad does it for us. The equivalent code with \texttt{Reader}s would be:

\begin{minted}[frame=single,framesep=4pt, fontfamily=tt, escapeinside=||, mathescape=true, linenos]{haskell}
data Environment = Environment
  {
    param1 :: Int,
    param2 :: String,
    ...
  }

loadEnv :: IO Environment
loadEnv = ...

func1 :: Environment -> Int
func1 env = param1 env

func2 :: Reader Environment String
func2 = do
    env <- ask
    let res = func1 env
    return ("Result: " ++ show res)

main :: IO ()
main = do
    env <- loadEnv
    let str = runReader func2 env
    print str
\end{minted}

Notice that we use the \texttt{runReader} function to call a reader, which takes a \texttt{Reader} value and its environment and returns the end result of that \texttt{Reader}. Within the \texttt{Reader} type, we can use the \texttt{ask} function to obtain the global data.

Remember that the data linked to the \texttt{Reader} type is read-only, so you can't modify it!

\headerspace{}

\subsection{\texttt{Writer}}

The \texttt{Writer} monad is similar to the \texttt{Reader} monad, except that it corresponds to an \emph{append}-only global ``variable''. For example, it can be useful for storing logs or acccumulated ``costs'' for functions that have been executed.

\begin{minted}[frame=single,framesep=4pt, fontfamily=tt, escapeinside=||, mathescape=true, linenos]{haskell}
import Control.Monad.Writer

logNumber :: Int -> Writer [String] Int
logNumber x = do
    tell ["Received number: " ++ show x]
    return x

multWithLog :: Writer [String] Int
multWithLog = do
    a <- logNumber 3
    b <- logNumber 5
    tell ["Gonna multiply these two numbers together..."]
    return (a*b)

main :: IO ()
main = print (runWriter multWithLog)
\end{minted}

Compared to the \texttt{Reader} type, the \texttt{Writer} monad returns a tuple \texttt{(x, state)}, where \texttt{x} is the ``pure'' part of the end result and \texttt{state} is the end result of the global variable. We run a \texttt{Writer} with \texttt{runWriter} and log data from a \texttt{Writer} with \texttt{tell}.
\headerspace{}

\subsection{\texttt{State}}

The \texttt{State} monad describes functions which take an argument and a state, and return a tuple containing a result and an updated state. In a way, it combines the \texttt{Reader} and \texttt{Writer} monads, \href{https://www.reddit.com/r/haskell/comments/7gl2wh/when_to_use_reader_vs_state}{with its own advantages and disadvantages}.

\begin{minted}[frame=single,framesep=4pt, fontfamily=tt, escapeinside=||, mathescape=true, linenos]{haskell}
import Control.Monad.State

playGame :: [String] -> State Int Int
playGame []         = do
    score <- get
    return score
playGame ("win":xs) = do
    score <- get
    put (score + 1)
    playGame xs
playGame ("lose":xs) = do
    score <- get
    put (score - 1)
    playGame xs
playGame (_:xs) = do
    playGame xs

startScore :: Int
startScore = 0

main = print (evalState (playGame ["win", "lose", "win", "win", "hello"]) startScore)
\end{minted}

We call a program using the \texttt{evalState} function, which takes a \texttt{State} monad and a starting state and returns a final state. Within the \texttt{State} monad, we can use the \texttt{get} command to retrieve the function's state, and \texttt{put} to update its state.

\exercise{\begin{enumerate}
    \item Using the \texttt{State} monad, write a program that takes as input a series of integers (line by line) from the user, followed by the string \texttt{done}, and then prints the highest integer entered.
    \item Using the \texttt{State} monad, write a function \texttt{leastCommonCharacter} that takes as input a string and outputs the least common character that occurs in the string (your choice, if tied).
    \item Try running a \texttt{State} monad multiple times with the same state. Does the second monad use the same state that was updated by the first? If not, how would you modify your code to reuse the state?
\end{enumerate}}
\headerspace{}

\subsection{\texttt{List}}

The great thing about \texttt{List} in Haskell is that it's also a monad: \texttt{List a} is actually syntactic sugar for \texttt{[a]} (yes, really!).

The \texttt{return} function for lists (see later) returns that element as a singleton list:

\begin{minted}[frame=single,framesep=4pt, fontfamily=tt, escapeinside=||, mathescape=true, linenos]{haskell}
itemAsList :: a -> [a]
itemAsList x = return x
-- is equivalent to:
-- itemAsList x = [x]
\end{minted}

And retrieving a ``pure'' value for lists returns a special function which, when used, applies that operation on every value on the list!

\begin{minted}[frame=single,framesep=4pt, fontfamily=tt, escapeinside=||, mathescape=true, linenos]{haskell}
squares lst = do
    x <- lst
    return (x * x)

main = print (squares [1, 2, 3])
\end{minted}

However, using lists as a monadic type generally results in difficult-to-read code, and, in any case, you shouldn't need to do this in everyday use.
\newpage

\section{How to use monads}

\subsection{Combining monads}

We can combine monads using the \emph{then} (\texttt{>>}) and \emph{bind} (\texttt{>>=}) operators to execute the side-effects of the monads in sequence.

The \emph{then} operator takes two monadic expressions and computes them sequentially, discarding the result of the first and returning the result of the second.

\begin{minted}[frame=single,framesep=4pt, fontfamily=tt, escapeinside=||, mathescape=true, linenos]{haskell}
getUserName :: () -> IO String
getUserName _ =
    putStr "Please enter your name: " >>        -- IO ()
        getLine                                 -- IO String
\end{minted}

Meanwhile, the bind operator computes the first monad, then pipes its output as the last input to the second monad (which must be a function).

\begin{minted}[frame=single,framesep=4pt, fontfamily=tt, escapeinside=||, mathescape=true, linenos]{haskell}
greetUser :: String -> IO ()
greetUser name = putStr ("Hello " ++ name ++ "!")

getUserNameAndGreet :: () -> IO ()
getUserNameAndGreet () =
    getUserName () >>=                          -- IO String
        greetUser                               -- String -> IO ()

main :: IO ()
main = do
    getUserNameAndGreet ()
\end{minted}

Think of them as the Haskell equivalents of \texttt{;} and \texttt{|} in Bash.
\newpage

\subsection{\texttt{do}}

We can simplify the above notation by using the \texttt{do} construct. When doing so, we can use the \texttt{<-} operator to assign the results of computations to temporary ``variables''. We can also use the \texttt{;} operator to separate computations (but we don't have to).

\begin{minted}[frame=single,framesep=4pt, fontfamily=tt, escapeinside=||, mathescape=true, linenos]{haskell}
getUserNameLength :: () -> IO Int
getUserNameLength _ = do
    putStr "Please enter your name: "
    name <- getLine
    return (length name)

main :: IO ()
main = do
    x <- nameReturn
    putStrLn x
\end{minted}

In fact, the following \texttt{main} functions are equivalent:

\begin{minted}[frame=single,framesep=4pt, fontfamily=tt, escapeinside=||, mathescape=true, linenos]{haskell}
main :: IO ()
-- main = do
--    x <- getUserName
--    getUserName x

main = do
    getUserName >>= print
\end{minted}

Care should be taken when using \texttt{do}, however, as it can result in ambiguous and difficult-to-read expressions. \href{https://wiki.haskell.org/Haskell_programming_tips#do_notation}{The Haskell Wiki advises against overuse of \texttt{do}.}

\exercise{\begin{enumerate}
    \item Write a function \texttt{myParseInt} that takes as input a string and returns the longest integer that can be found at the start of the string, or \texttt{Nothing} if there is no integer.
    \item Use your \texttt{myParseInt} function to write a function that prints \emph{half} the integer found at the start of the string. Do this using both the bind operator and \texttt{do} notation. Which feels more intuitive to you? Which do you prefer?
    \item Rewrite the \texttt{getUserNameLength} function in Section 3.1 using the bind operator.
\end{enumerate}}
\newpage

\subsection{Retrieving values from monads}

As we've seen earlier, there are two operators that allow us to utilise the results of monads: the \texttt{>>=} and \texttt{<-} operators. However, remember that these also produce monadic types, meaning they can't be used in ``pure'' code.

\begin{minted}[frame=single,framesep=4pt, fontfamily=tt, escapeinside=||, mathescape=true, linenos]{haskell}
-- Print the first character of a String and return the rest of it
printFirstChar :: String -> IO String
printFirstChar ""     = return ""
printFirstChar (x:xs) = do
    putChar x
    return xs

main :: IO ()
main =  do
    x <- printFirstChar "Hhhello, world!"
    print '\n'
    print x
    return ()
\end{minted}
\headerspace{}

\subsection{Turning pure values into monads}

What happens when we have a pure value and want it to become monadic? We can declare it as being monadic by using the \texttt{return} statement:

\begin{minted}[frame=single,framesep=4pt, fontfamily=tt, escapeinside=||, mathescape=true, linenos]{haskell}
f :: Int -> Int
f n = n + 3

g :: Int -> IO Int
g n = return (n + 3)
\end{minted}
\newpage

\subsection{Making code more concise}

We can also use monadic binding to make code more concise. For example, the Haskell Wiki lists \href{href{https://wiki.haskell.org/Maybe}}{the following example}, which uses the \texttt{Maybe} as a monad to make \texttt{h'} (slightly) more concise than \texttt{h}: is a monad as well as a type, we can use the monadic operators with it to make our code even more concise and easier to read. For example, \:

\begin{minted}[frame=single,framesep=4pt, fontfamily=tt, escapeinside=||, mathescape=true, linenos]{haskell}
f :: Int -> Maybe Int
f 0 = Nothing
f x = Just x

g :: Int -> Maybe Int
g 100 = Nothing
g x = Just x

h :: Int -> Maybe Int
h x = case f x of
    Just n -> g n
    Nothing -> Nothing

h' :: Int -> Maybe Int
h' x = do n <- f x
    g n
\end{minted}

In this case, using the \texttt{<-} operator only provides a reduction of a single line of code, but the difference in code size becomes much more noticeable when we combine multiple functions.

\section{Building a CLI calculator}

\exercise{\begin{enumerate}
    \item Write a CLI calculator program in Haskell. The program should read lines of input from \texttt{stdin}, and evaluate their results. Your program should support the \texttt{+}, \texttt{-}, \texttt{*} and \texttt{/} operations.
    \item Extend your program so that users can omit the first argument (eg. by typing \texttt{+3}), in which case the most recently calculated result is used as the first argument. Your program should also support a \texttt{back} command to remove the most recently stored result from memory, and a \texttt{clear} command to clear the program's memory.
    \item Extend your program so that it also handles invalid inputs.
\end{enumerate} \ \vspace{0.2cm} (\emph{Hint: what monads are useful here?})}

\section{Coffee break}

Try to run the below code. What goes wrong and why? Try to understand the error messages you get and modify the code so that it runs successfully.

\begin{minted}[frame=single,framesep=4pt, fontfamily=tt, escapeinside=||, mathescape=true, linenos]{haskell}
getUserNameLength :: () -> IO Int
getUserNameLength _ = do
    putStr "Please enter your name: "
    name <- getLine
    return (length name)
    return 3

main :: IO ()
main = do
    x <- getUserNameLength ()
    print x
\end{minted}

\section{Using GTK+}

Haskell supports a number of libraries to build GUI applications, including wx, X11, hsqml, fltkhs and GTK+.

\exercise{\begin{enumerate}
    \item \href{https://haskell-at-work.com/gtk-programming-with-haskell}{Learn how GTK+ works by following the tutorial here.}
    \item (\emph{Extension}) Use GTK+ to develop a graphical implementation of your calculator program. If you need help, \href{https://www.stackbuilders.com/blog/gui-application}{follow the tutorial here}.
\end{enumerate}}

\section{Next time}

Congratulations if you've made it this far! We've learnt how functional programming works, and used an advanced functional language to build a program with all the features we expect in any other programming language. In the following weeks, we'll be looking at one more language, Rust, and see how it combines these features to enable fast, safe low-level software.
\newpage

\section{Further reading}

\begin{enumerate}
    \item \href{https://leanpub.com/book-of-monads}{\textit{Book of Monads}} by Alejandro Serrano Mena
    \item \href{https://wiki.haskell.org/Monads_as_computation}{\textit{Monads as computation}} on the Haskell Wiki
    \item \href{https://en.wikibooks.org/wiki/Haskell/Understanding_monads}{\textit{Understanding monads}} on the Haskell Wiki
    \item \href{https://elbear.com/how-to-use-monads-without-understanding-them.html}{\textit{How to Use Monads without Understanding Them}.} Blog post by lucian on Monads
    \item \href{https://mmhaskell.com/monads/reader-writer}{\textit{Reader and Writer Monads} by Monday Morning Haskell}
    \item \href{https://learnyouahaskell.com/for-a-few-monads-more}{\textit{For a Few Monads More} by Learn You a Haskell}
    \item \href{https://flaviocorpa.com/}{\textit{Haskell for Elm Developers}.} Tutorial on Monads by Flavio Corpa
\end{enumerate}

\end{document}
