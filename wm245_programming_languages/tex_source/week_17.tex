\documentclass[11pt,oneside,a4paper]{article}
\usepackage[%
	a4paper,%
	left = 20mm,%
	right = 20mm,%
	textwidth = 178mm,%
	top = 35mm,%
	bottom = 30mm,%
	headheight=70pt,%
headsep=25pt,%
]{geometry}
\usepackage{graphicx}
\usepackage[inkscapeformat=pdf]{svg}
\usepackage{transparent}
\usepackage{lastpage}

\usepackage{fontspec}
\setmainfont{Montserrat}
\setmainfont{Noto Sans}

\input{helper/preamble.tex}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Edit below
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\def\weekno{17}
\def\labtitle{The Rust Programming Language}
\input{module_info}

\setmainfont[
    UprightFont = {Montserrat Light},
    BoldFont = {Montserrat Bold},
    ItalicFont = {Montserrat Italic},
    BoldItalicFont = {Montserrat Bold Italic}
]{Montserrat}

\input{helper/header.tex}

\title{Week \weekno{}: \labtitle{}}
\author{\moduleno{} \moduletitle{}}
\date{January 2024}

\begin{document}

\begin{center}
    \LARGE{\textbf{\moduleno{}: \moduletitle{}}}

    \Large{Week \weekno{}: \labtitle{}}

    \vspace{0.5cm}

    \includegraphics[height=4cm]{img/ferris_pride.png}

    \vspace{0.5cm}
\end{center}

Rust is a relatively new language, having been first released in 2015, and has been a favourite among programmers ever since for its emphasis on speed and memory safety. Over the next two weeks, you will learn the basics of Rust, finding out why it is one of the programming community's favourite languages.

\section{What is Rust?}

Rust is a low-level or systems programming language, distinguished by its ability to interact directly with hardware without requiring intermediary abstraction layers.

The impetus for creating Rust came from a frustration experienced by its creator, Graydon Hoare, who noticed the recurring software crashes in the lift system of his apartment. These crashes were often attributed to memory bugs typical in system-level programs coded in languages like C or C++.

Motivated by this challenge, Hoare embarked on a side project to develop a new language that retained the compactness and speed associated with traditional system languages while mitigating the prevalence of memory bugs. This project culminated in the creation of Rust, which has since gained recognition for its safety, performance, and suitability for modern software development.

\begin{figure}
    \centering
    \includegraphics[width=0.5\linewidth]{img/ferris.png}
    \caption{Ferris is the unofficial mascot for Rust!}
    \label{fig:ferris}
\end{figure}

Rust addresses this challenge by enabling memory-safe code without the performance overhead of runtime garbage collection or relying on developers to manage redundant objects consuming memory. This approach minimises human error and reduces the likelihood of bugs that lead to software crashes and security vulnerabilities. Unlike languages such as Java, JavaScript, and Python, which use garbage collectors to automate memory cleanup during runtime, Rust aims to provide precise control and high performance while automating memory allocation.

\section{Where is Rust used?}

The applications of Rust can be found across a diverse array of domains. One prominent use case for Rust is in developing performance-critical backend systems, where its performance, thread safety, and robust error handling make it an ideal choice. Major platforms like GitHub leverage Rust for critical components, such as the backend of their code search feature.

Operating system development also benefits from Rust's capabilities, with projects like Redox, a Unix-like operating system, that uses Rust for its security and performance advantages. Similarly, Rust is well-suited for writing code that interacts closely with the operating system, as seen in projects like GitHub Codespaces and Coursera's online grading system.

The language's suitability for web development is increasingly recognised, especially for server-side applications. Rust's asynchronous programming model and performance characteristics make it ideal for building high-performance web servers and backend services. Web frameworks like Rocket further strengthens Rust's position in this domain.

Rust's speed, memory management, and security features make it a preferred choice for cryptocurrency and blockchain development. Projects like Polkadot leverage Rust for building core infrastructure and runtime logic, ensuring the security and decentralisation of blockchain networks.

\section{Why Rust?}

Rust has maintained its position as the most coveted programming language for the eighth consecutive year, according to Stack Overflow's annual developer survey. This remarkable streak, and with over 80\% of developers expressing a desire to continue using Rust in the coming year, highlights the language's widespread appeal and adoption within the developer community.

\vspace{1cm}

Here are a few attractive characteristics and features of Rust:
\begin{enumerate}
    \item \textbf{Memory safety:} Rust revolutionises memory safety by leveraging its ownership system and strict compile-time checks.
    \item \textbf{Concurrency:} Rust includes features for concurrent programming via its ownership system, which enforces strict regulations on data access, and its borrowing model, which manages access to data to prevent conflicts. This setup guarantees that multiple threads can collaborate on shared data without causing memory-related problems.
    \item \textbf{Performance:} Rust delivers high performance comparable to C and C++ by providing low-level control over memory management and resource allocation. Rust's zero-cost abstractions and efficient runtime enable developers to write code that runs fast without sacrificing safety or productivity.
    \item \textbf{Cargo package manager:} Cargo is Rust's package manager and build system, designed to streamline the development process and manage dependencies efficiently.
\end{enumerate}

There is in fact a \href{https://brson.github.io/fireflowers/} {website} composed of developers' praises for Rust.

\section{Key Terminology}

Before beginning to look at Rust code, it is important to familiarise with some common Rust terminology:
\begin{itemize}
    \item \textbf{Ownership} is the mechanism by which memory is managed. Each value in Rust has a single owner, and when the owner goes out of scope, the value is deallocated. This prevents issues like memory leaks and data races.
    \item \textbf{Borrowing} allows code to temporarily access a value without taking ownership of it.
    \item \textbf{References} are similar to pointers in other languages, but with additional compile-time checks to ensure safety. They allow code to access values without taking ownership, enabling functions to work with data without consuming it.
    \item \textbf{Mutability} defines if variables can be changed after they are created. Mutable variables can be changed after they are created, while immutable variables cannot be modified.
    \item \textbf{Traits} define behavior that types can implement. They are similar to interfaces or protocols in other languages and allow for code reuse and polymorphism.
    \item \textbf{Enums} (enumerations) allow developers to define a type that can hold one of several possible values. Enums are commonly used in Rust to represent variants or options, making code more expressive and easier to understand.

\end{itemize}

\section{Setting Up Development Environment}
To install Rust, visit the official \href{https://www.rust-lang.org/} {Rust website}.
Follow the instructions to download and install the Rust compiler and toolchain for your operating system.

To verify the installation, open a terminal or command prompt.
Type \texttt{rustc --version} and press Enter to check if Rust is installed properly. You should see the version number of the Rust compiler.

Cargo is Rust's package manager and build system. It comes bundled with Rust, so if you've installed Rust, you should have Cargo as well.
Verify the installation by typing \texttt{cargo --version} in the terminal.

Choose an integrated development environment (IDE) for writing Rust code. Some popular options include Eclipse IDE, IntelliJ IDEA , or Atom.
Install the necessary Rust-related extensions or plugins for your chosen editor to get syntax highlighting, code completion, and other Rust-specific features.

To create a new Rust project, open a terminal or command prompt. Navigate to the directory where you want to create your Rust project. Use Cargo to create a new Rust project by running \texttt{cargo new\_project\_name}.

Then, open your IDE and navigate to the directory containing your Rust project. Open the src/main.rs file in your editor.
Write your Rust code in the main.rs file.

In the terminal, navigate to your project directory. Run \texttt{cargo build} to compile your Rust program. If there are no errors, run \texttt{cargo run} to build and execute your Rust program. You should see the output of your program in the terminal.

\section{Basic Syntax and Concepts}
Variables in Rust are declared using the let keyword. By default, variables are immutable, meaning their values cannot be changed once set. Below is an example:

\begin{minted}[frame=single,framesep=4pt, fontfamily=tt, escapeinside=||, mathescape=true, linenos]{rust}
let x = 5; // Immutable variable
\end{minted}

In this example, x is an immutable variable with the value 5.

Rust has several primitive data types, including integers, floating-point numbers, booleans, characters, and strings. Here are examples of each:

\begin{minted}[frame=single,framesep=4pt, fontfamily=tt, escapeinside=||, mathescape=true, linenos]{rust}
let integer: i32 = 42;        // 32-bit signed integer
let float: f64 = 3.14;        // 64-bit floating-point number
let boolean: bool = true;     // Boolean (true or false)
let character: char = 'A';    // Character
let string: String = String::from("Hello, Rust!"); // String (dynamic string)
\end{minted}

To create a mutable variable in Rust, you use the let mut syntax. Mutable variables can be changed after they are declared. Below is an example:

\begin{minted}[frame=single,framesep=4pt, fontfamily=tt, escapeinside=||, mathescape=true, linenos]{rust}
let integer: i32 = 42;        // 32-bit signed integer
let float: f64 = 3.14;        // 64-bit floating-point number
let boolean: bool = true;     // Boolean (true or false)
let character: char = 'A';    // Character
let string: String = String::from("Hello, Rust!"); // String (dynamic string)
\end{minted}

To create a mutable variable in Rust, you use the \texttt{let mut} syntax. Mutable variables can be changed after they are declared. Below is an example:

\begin{minted}[frame=single,framesep=4pt, fontfamily=tt, escapeinside=||, mathescape=true, linenos]{rust}
let mut y = 10; // Mutable variable
y = 20;         // Updating the value of y
\end{minted}

In this example, y is a mutable variable initially set to 10, but later its value is changed to 20.

Constants are declared using the \texttt{const} keyword and must have a specified type. Constants are immutable by default, and their values cannot be changed during the program's execution. Below is an example:

\begin{minted}[frame=single,framesep=4pt, fontfamily=tt, escapeinside=||, mathescape=true, linenos]{rust}
const MAX_POINTS: u32 = 10000; // Constant with type annotation
\end{minted}

In this example, MAX\_POINTS is a constant of type u32 (unsigned 32-bit integer) with the value 10000. The value of MAX\_POINTS cannot be modified.

Functions in Rust are declared using the \texttt{fn} keyword. Functions can have parameters and return values. For example:

\begin{minted}[frame=single,framesep=4pt, fontfamily=tt, escapeinside=||, mathescape=true, linenos]{rust}
// Function with parameters and return value
fn add(a: i32, b: i32) -> i32 {
    // Adding two numbers and returning the result
    return a + b;
}

fn main() {
    // Calling the add function with arguments 5 and 3
    let result = add(5, 3);
    // Printing the result
    println!("The result of addition is: {}", result);
}
\end{minted}

\exercise{\begin{enumerate}
\item Write a function called \texttt{multiply} that takes two parameters (a and b) of type i32 and returns their product.
Call the \texttt{multiply} function with different pairs of numbers and print the results.
\item Write a function called \texttt{calculate\_area} that takes two parameters (length and width) of type f64 and returns the area of a rectangle.
Call the \texttt{calculate\_area} function with different values for length and width and print the results.
\item Write a function called \texttt{celsius\_to\_fahrenheit} that takes a parameter celsius of type f64 (floating-point number) representing temperature in Celsius and returns the equivalent temperature in Fahrenheit.
Call the \texttt{celsius\_to\_fahrenheit} function with different temperatures and print the results.
\end{enumerate}}

\section{Control Flow}
Conditional statements allow you to execute different blocks of code based on the evaluation of a condition. In Rust, the \texttt{if}, \texttt{else if}, and \texttt{else} keywords are used for conditional branching.

Consider the example below:

\begin{minted}[frame=single,framesep=4pt, fontfamily=tt, escapeinside=||, mathescape=true, linenos]{rust}
let number = 42;

if number < 0 {
    println!("Number is negative");
} else if number == 0 {
    println!("Number is zero");
} else {
    println!("Number is positive");
}
\end{minted}

Loops allow you to execute a block of code repeatedly until a certain condition is met, including loop, while, and for loops.

Consider the example below:

\begin{minted}[frame=single,framesep=4pt, fontfamily=tt, escapeinside=||, mathescape=true, linenos]{rust}
// Infinite loop
let mut count = 0;
loop {
    println!("Count: {}", count);
    count += 1;
    if count == 5 {
        break; // Exit loop when count reaches 5
    }
}

// While loop
let mut i = 0;
while i < 5 {
    println!("Value of i: {i}");
    i += 1;
}

// For loop (using range)
for j in 0..5 {
    println!("Value of j: {j}");
}
\end{minted}

\exercise{\begin{enumerate}
\item Write a program that calculates the sum of all even numbers from 1 to 100 using a loop. Print the result.
\item Write a program that generates the Fibonacci series up to a given number of terms.
\end{enumerate}}

\section{Ownership and Borrowing}
Ownership is Rust's mechanism for managing memory and resources. Every value in Rust has a single owner, and the owner is responsible for deallocating the memory when it goes out of scope.

To comprehend this concept, consider the example below:

\begin{minted}[frame=single,framesep=4pt, fontfamily=tt, escapeinside=||, mathescape=true, linenos]{rust}
let s = String::from("hello"); // s is the owner of the String
println!("{}", s);             // s is still valid here

// Ownership of s is transferred to s2
let s2 = s;

// Error! s is no longer valid because its ownership was transferred
println!("{}", s);             // This will result in a compilation error
\end{minted}

Borrowing allows code to temporarily borrow references to values without taking ownership. There are two types of borrowing in Rust: immutable borrowing (\&) and mutable borrowing (\&mut). Borrowing ensures that only one part of the code can modify data at a time, preventing data races.

To fully grasp this concept please refer to the example below:
\begin{minted}[frame=single,framesep=4pt, fontfamily=tt, escapeinside=||, mathescape=true, linenos]{rust}
fn print_length(s: &String) {
    println!("Length of the string: {}", s.len());
}

let s = String::from("hello");
print_length(&s); // Immutable borrowing

let mut s = String::from("hello");
modify_string(&mut s); // Mutable borrowing

\end{minted}

\exercise{\begin{enumerate}
\item Write a function that takes a string as input and returns its length. Use borrowing to pass the string to the function without taking ownership.
\item Write a function that takes a string and two indices as input and returns a substring consisting of the characters between the specified indices. Use borrowing to pass the string to the function.
\end{enumerate}}

\section{Further reading}

\begin{enumerate}
    \item \href{https://doc.rust-lang.org/book}{\textit{The Rust Book}} by Steve Klabnik and Carol Nichols
    \item \href{https://www.rust-lang.org/learn}{\textit{Learn Rust}}, the official references and tutorials for Rust
    \item \href{https://snake.rustbridge.com/}{\textit{The Rusty Snake Book},} a tutorial on Rust by Rustbridge
    \item \href{https://rust-quest.com}{\textit{Rust Quest},} a tutorial on Rust by Liam Garriga
    \item \href{https://rustacean.net}{\textit{Website for Ferris the Crab}}
\end{enumerate}

\end{document}
