# WM242: Implementing Secure Systems

> *See more: https://warwick.ac.uk/fac/sci/wmg/education/undergraduate/cyber/modules/iss*

Secure systems have a singular goal - to concurrently enable things to happen that should happen, whilst preventing things from happening that should not happen. Within that simplicity are deep challenges: defining the contextually contingent sets of should and should not; anticipating what the future might bring; determining the extent of the system.

This module is concerned with deliberately choosing good patterns of implementation for the long-term well being of the system.

# Contents

This module covers:
* Design and development considerations
* Selecting and applying core technologies
* Recognising security needs on, across and between platforms
* Cryptography
* Network security
* Human factors
* Security systems development
