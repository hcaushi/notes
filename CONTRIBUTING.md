# Contributing

I welcome suggestions and improvements. Since many of the files were compiled in MS Word, the only way of suggesting changes to those files is currently by [submitting an issue](https://gitlab.com/hcaushi/notes/-/issues), but I hope to eventually move to a markup file standard so that pull requests can be submitted directly to the repo.
